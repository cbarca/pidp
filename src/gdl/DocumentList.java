package gdl;

import gdl.DocumentListException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.google.gdata.client.authn.oauth.GoogleOAuthHelper;
import com.google.gdata.client.authn.oauth.GoogleOAuthParameters;
import com.google.gdata.client.authn.oauth.OAuthException;
import com.google.gdata.client.authn.oauth.OAuthHmacSha1Signer;
import com.google.gdata.client.authn.oauth.OAuthRsaSha1Signer;
import com.google.gdata.client.authn.oauth.OAuthSigner;
import com.google.gdata.client.docs.DocsService;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.acl.AclEntry;
import com.google.gdata.data.acl.AclRole;
import com.google.gdata.data.acl.AclScope;
import com.google.gdata.data.docs.DocumentEntry;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.docs.DocumentListFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

/**
 * DocumentList class - singleton class
 * Uses Google Document List Api for document sharing
 * @author Cristi
 *
 */
public class DocumentList {
	// Constants
	// STEP 1: Configure how to perform OAuth 1.0a
	public final static boolean USE_RSA_SIGNING = false;
	public final static String CONSUMER_KEY = "1029380298091.apps.googleusercontent.com";
	public final static String CONSUMER_SECRET = "OjGQH_odFXyvLdUxohO2V0W4";
	public final static String SCOPES = "https://docs.google.com/feeds/ https://docs.googleusercontent.com/";
	public final static String DEFAULT_HOST = "docs.google.com";
		
	// Private members
	private DocsService service;
	private GoogleOAuthHelper oauthHelper;
	private GoogleOAuthParameters oauthParameters;
	private OAuthSigner signer;
	private HashMap<String, DocumentListEntry> docHash;
	
	// Singleton object
	private static DocumentList docList = null;
	
	/**
	 * 
	 * @return
	 */
	public static DocumentList getInstance() {
		if (docList == null) {
			try {
				docList = new DocumentList();
			} catch (OAuthException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
		return docList;
	}
	
	/**
	 * Default constructor
	 * @throws OAuthException
	 */
	public DocumentList() throws OAuthException {
		// STEP 2: Set up the OAuth objects
		// You first need to initialize a few OAuth-related objects.
		// GoogleOAuthParameters holds all the parameters related to OAuth.
		// OAuthSigner is responsible for signing the OAuth base string.
		oauthParameters = new GoogleOAuthParameters();

		// Set your OAuth Consumer Key (which you can register at
		// https://www.google.com/accounts/ManageDomains).
		oauthParameters.setOAuthConsumerKey(CONSUMER_KEY);

		// Initialize the OAuth Signer.  If you are using RSA-SHA1, you must provide
		// your private key as a Base-64 string conforming to the PKCS #8 standard.
		// Visit http://code.google.com/apis/gdata/authsub.html#Registered to learn
		// more about creating a key/certificate pair.  If you are using HMAC-SHA1,
		// you must set your OAuth Consumer Secret, which can be obtained at
		// https://www.google.com/accounts/ManageDomains.
		if (USE_RSA_SIGNING){
			signer = new OAuthRsaSha1Signer(CONSUMER_SECRET);
		} else {
			oauthParameters.setOAuthConsumerSecret(CONSUMER_SECRET);
			signer = new OAuthHmacSha1Signer();
		}

		// Finally, create a new GoogleOAuthHelperObject.  This is the object you
		// will use for all OAuth-related interaction.
		oauthHelper = new GoogleOAuthHelper(signer);
		
		// STEP 3: Get the Authorization URL
		// Set the scope for this particular service.
		oauthParameters.setScope(SCOPES);

		// This method also makes a request to get the unauthorized request token,
		// and adds it to the oauthParameters object, along with the token secret
		// (if it is present).
		oauthHelper.getUnauthorizedRequestToken(oauthParameters);
		
		// Create a hashmap to keep the entries associated with their names
		docHash = new HashMap<String, DocumentListEntry>();
	}
	
	/**
	 * 
	 * @return
	 */
	public String getClientReqUrl() {
		// Get the authorization url.  The user of your application must visit
		// this url in order to authorize with Google.  If you are building a
		// browser-based application, you can redirect the user to the authorization
		// url.
		String requestUrl = oauthHelper.createUserAuthorizationUrl(oauthParameters);
		System.out.println(requestUrl);
				
		return requestUrl;
	}
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getServerReqUrl() throws IOException {
		// Get the authorization url.  The user of your application must visit
		// this url in order to authorize with Google.  If you are building a
		// browser-based application, you can redirect the user to the authorization
		// url.
		String requestUrl = oauthHelper.createUserAuthorizationUrl(oauthParameters);
		System.out.println(requestUrl);
		System.out.println("Please visit the URL above to authorize your OAuth "
				+ "request token.  Once that is complete, press any key to "
				+ "continue...");
		System.in.read();
		
		return requestUrl;
	}
	
	/**
	 * 
	 * @return
	 * @throws OAuthException
	 */
	public String getAccessToken() throws OAuthException {
		// STEP 4: Get the Access Token
		// Once the user authorizes with Google, the request token can be exchanged
		// for a long-lived access token.  If you are building a browser-based
		// application, you should parse the incoming request token from the url and
		// set it in GoogleOAuthParameters before calling getAccessToken().
		String token = oauthHelper.getAccessToken(oauthParameters);
		
		return token;
	}
	
	/**
	 * 
	 * @throws AuthenticationException
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ServiceException
	 * @throws OAuthException
	 */
	public void loginService()
		throws AuthenticationException, MalformedURLException, IOException, ServiceException, OAuthException {
		// STEP 5: Make an OAuth authorized request to Google
		// Initialize the variables needed to make the request
		System.out.println("Login Service...");
		service = new DocsService("DocumentsListIntegration-v1");
		service.setProtocolVersion(DocsService.Versions.V2);

		// Set the OAuth credentials which were obtained from the step above.
		service.setOAuthCredentials(oauthParameters, signer);
	}
	
	/**
	 * 
	 * @throws OAuthException
	 */
	public void logoutService() throws OAuthException {
		// STEP 6: Revoke the OAuth token
		System.out.println("Revoking OAuth Token...");
		oauthHelper.revokeToken(oauthParameters);
		System.out.println("OAuth Token revoked...");
	}
	
	/**
	 * Create a new item in the DocList.
	 *
	 * @param title the title of the document to be created.
	 * @param type the type of the document to be created. One of "spreadsheet",
	 *        "presentation", or "document".
	 *
	 * @throws DocumentListException
	 * @throws ServiceException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public DocumentListEntry createNewDoc(String title) throws MalformedURLException,
	IOException, ServiceException, DocumentListException {
		DocumentEntry newDocEntry = new DocumentEntry();
		newDocEntry.setTitle(new PlainTextConstruct(title));
		
		DocumentListEntry newDocListEntry = service.insert(new URL("https://docs.google.com/feeds/" +
				"documents/private/full/"), newDocEntry); 
		
		docHash.put(title, newDocListEntry);
		
		System.out.println("Document " + title + " created!");
		
		return newDocListEntry; 
	}
	
	/**
	 * Delete an item from the DocList
	 * @param title the title of the document to be deleted
	 * @throws IOException
	 * @throws ServiceException
	 */
	public void deleteDoc(String title) throws IOException, ServiceException {
		DocumentListEntry docListEntry = docHash.get(title);
		
		service.getRequestFactory().setHeader("If-Match", "*");	
		service.delete(new URL("https://docs.google.com/feeds/documents/private/full/" + 
				docListEntry.getResourceId()));
		
		docHash.remove(title);
		
		System.out.println("Document " + title + " deleted!");
		service.setHeader("If-Match", null);
	}
	
	/**
	 * Add an ACL role to an object.
	 *
	 * @param role the role of the ACL to be added to the object.
	 * @param scope the scope for the ACL.
	 * @param docListEntry the entry of the object to set the ACL for.
	 *
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws ServiceException
	 * @throws DocumentListException
	 */
	public AclEntry addAclRole(AclRole role, AclScope scope, DocumentListEntry docListEntry)
	throws IOException, MalformedURLException, ServiceException,
	DocumentListException {
		if (role == null || scope == null || docListEntry == null) {
			throw new DocumentListException("null passed in for required parameters");
		}

		AclEntry aclEntry = new AclEntry();
		aclEntry.setRole(role);
		aclEntry.setScope(scope);
		URL url = new URL("https://docs.google.com/feeds/acl/private/full/" + 
				docListEntry.getResourceId());

		System.out.println("Writer role for " + scope.getValue() + " added!");
		
		return service.insert(url, aclEntry);
	}
	
	/**
	 * Remove an ACL role from a object.
	 *
	 * @param scope scope of the ACL to be removed.
	 * @param email email address to remove the role of.
	 * @param docListEntry the entry of the object to remove the ACL for.
	 *
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws ServiceException
	 * @throws DocumentListException
	 */
	public void removeAclRole(String scope, String email, DocumentListEntry docListEntry) throws IOException,
	MalformedURLException, ServiceException, DocumentListException {
		if (scope == null || email == null || docListEntry == null) {
			throw new DocumentListException("null passed in for required parameters");
		}

		URL url = new URL("https://docs.google.com/feeds/acl/private/full/" + 
				docListEntry.getResourceId() 
				+ "/" + scope + "%3A" + email);

		service.getRequestFactory().setHeader("If-Match", "*");	
		service.delete(url);
		
		System.out.println("Writer role for " + email + " removed!");
		service.setHeader("If-Match", null);
	}
	
	/**
	 * Return the document entry with the requested title
	 * @return document list entry
	 * @throws IOException
	 * @throws ServiceException
	 */
	public DocumentListEntry retrieveDocListEntry(String title) throws IOException, ServiceException {
		URL feedUri = new URL("https://docs.google.com/feeds/documents/private/full/");
		DocumentListFeed feed = service.getFeed(feedUri, DocumentListFeed.class);

		for (DocumentListEntry docListEntry : feed.getEntries()) {
			if (docListEntry.getTitle().getPlainText().compareTo(title) == 0) {
				return docListEntry;
			}
		}
		
		return null;
	}

	/**
	 * Update document text in google doc
	 * @param title title of the document to be updated
	 * @param xmlFile name of the file where the whiteboard is saved
	 * 
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ServiceException
	 * @throws DocumentListException
	 */
	public void updateDocText(String title) throws MalformedURLException,
	IOException, ServiceException, DocumentListException {
		DocumentListEntry docListEntry = docHash.get(title);
		String xmlFile = title + "_messages.txt";
		
		DocumentEntry newDocEntry = new DocumentEntry();
		newDocEntry.setTitle(new PlainTextConstruct(title));
		
		File file = new File(xmlFile);
	    
	    String mimeType = DocumentListEntry.MediaType.fromFileName(file.getName()).getMimeType();
	    newDocEntry.setFile(file, mimeType);
	    
	    service.getRequestFactory().setHeader("If-Match", "*");	
		service.updateMedia(new URL("https://docs.google.com/feeds/" +
		"documents/private/full/" + docListEntry.getResourceId()), newDocEntry); 

		System.out.println("Document " + title + " updated!");
		service.setHeader("If-Match", null);
	}
	
	/**
	 * Get doc hash instance
	 * @return hash map instance of doc hash
	 */
	public HashMap<String, DocumentListEntry> getDocHash() {
		return docHash;
	}
}
