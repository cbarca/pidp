package net;


import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.SimpleLayout;


import mediator.Mediator;
import net.command.AddNewGroup;
import net.command.AddUserToGroup;
import net.command.DrawShapeToGroup;
import net.command.ExcludeUser;
import net.command.IncludeUser;
import net.command.LoginUser;
import net.command.ReqLoginUserAck;
import net.command.SendMessageToGroup;
import net.command.UserLeaveGroup;
import net.controller.ChangeRequest;
import net.controller.ClientWorker;
import net.controller.Invoker;
import net.model.ECmdMessage;

public class Client implements Runnable {
	// The host:port combination to connect to
	private InetAddress hostAddress;
	private int port;

	// The selector we'll be monitoring
	private Selector selector;

	// The buffer into which we'll read data when it's available
	private ByteBuffer readBuffer = ByteBuffer.allocate(65536);

	// A list of PendingChange instances
	private List pendingChanges = new LinkedList();

	// Maps a SocketChannel to a list of ByteBuffer instances
	private Map pendingData = new HashMap();
	
	SocketChannel socket;
	
	private ClientWorker worker;
	
	private Mediator mediator;
	
	private Invoker invoker;
	
	private Logger logger;
	
	public Client(InetAddress hostAddress, int port, Mediator mediator, ClientWorker worker) throws IOException {
		this.hostAddress = hostAddress;
		this.port = port;
		this.mediator = mediator;
		this.worker = worker;
		this.selector = this.initSelector();
		this.socket = this.initiateConnection();
		
		invoker = new Invoker();
		
		//create logging file
		FileAppender appender;
		//username provided at runtime
		if (mediator.getUsername() != null)
			appender = new FileAppender(new SimpleLayout(), mediator.getUsername() + ".log");
		else
			appender = new FileAppender(new SimpleLayout(), "default.log");
		appender.activateOptions();
		appender.setLayout(new PatternLayout("%-4r [%t] %-5p %c %x - %m%n"));
		
		logger = Logger.getRootLogger();
		logger.addAppender(appender);
		
		logger.info("Client created");
		
		// Store commands
		invoker.storeCmd(new AddNewGroup(ECmdMessage.AddNewGroup, 2, mediator));
		invoker.storeCmd(new AddUserToGroup(ECmdMessage.AddUserToGroup, 5, mediator));
		invoker.storeCmd(new ExcludeUser(ECmdMessage.ExcludeUser, 1, mediator));
		invoker.storeCmd(new IncludeUser(ECmdMessage.IncludeUser, 1, mediator));
		invoker.storeCmd(new LoginUser(ECmdMessage.LoginUser, 1, mediator));
		invoker.storeCmd(new UserLeaveGroup(ECmdMessage.UserLeaveGroup, 2, mediator));
		invoker.storeCmd(new ReqLoginUserAck(ECmdMessage.ReqLoginUserAck, 1, mediator));
		invoker.storeCmd(new DrawShapeToGroup(ECmdMessage.DrawShapeToGroup, 5, mediator));
		invoker.storeCmd(new SendMessageToGroup(ECmdMessage.SendMessageToGroup, 3, mediator));
	}

	public Client(InetAddress hostAddress, int port, ClientWorker worker) throws IOException {
		this.hostAddress = hostAddress;
		this.port = port;
		this.mediator = new Mediator();
		this.worker = worker;
		this.selector = this.initSelector();
		this.socket = this.initiateConnection();
		
		invoker = new Invoker();
		
		//create logging file
		FileAppender appender;
		//username provided at runtime
		if (mediator.getUsername() != null)
			appender = new FileAppender(new SimpleLayout(), mediator.getUsername() + ".log");
		else
			appender = new FileAppender(new SimpleLayout(), "default.log");
		appender.activateOptions();
		appender.setLayout(new PatternLayout("%-4r [%t] %-5p %c %x - %m%n"));
		
		logger = Logger.getRootLogger();
		logger.addAppender(appender);
		
		logger.info("Client created");
		
		// Store commands
		invoker.storeCmd(new AddNewGroup(ECmdMessage.AddNewGroup, 2, mediator));
		invoker.storeCmd(new AddUserToGroup(ECmdMessage.AddUserToGroup, 5, mediator));
		invoker.storeCmd(new ExcludeUser(ECmdMessage.ExcludeUser, 1, mediator));
		invoker.storeCmd(new IncludeUser(ECmdMessage.IncludeUser, 1, mediator));
		invoker.storeCmd(new LoginUser(ECmdMessage.LoginUser, 1, mediator));
		invoker.storeCmd(new UserLeaveGroup(ECmdMessage.UserLeaveGroup, 2, mediator));
		invoker.storeCmd(new ReqLoginUserAck(ECmdMessage.ReqLoginUserAck, 1, mediator));
		invoker.storeCmd(new DrawShapeToGroup(ECmdMessage.DrawShapeToGroup, 5, mediator));
		invoker.storeCmd(new SendMessageToGroup(ECmdMessage.SendMessageToGroup, 3, mediator));
	}

	/**
	 * Queue message to be sent to the server
	 * @param data
	 */
	public void send(byte[] data) {
		synchronized (this.pendingChanges) {
			// Indicate we want the interest ops set changed
			this.pendingChanges.add(new ChangeRequest(socket, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

			logger.debug("Send message:" + new String(data));
			
			// And queue the data we want written
			synchronized (this.pendingData) {
				List<ByteBuffer> queue = (List<ByteBuffer>) this.pendingData.get(socket);
				if (queue == null) {
					queue = new ArrayList<ByteBuffer>();
					this.pendingData.put(socket, queue);
				}
				queue.add(ByteBuffer.wrap(data));
			}
		}

		// Finally, wake up our selecting thread so it can make the required changes
		this.selector.wakeup();
	}

	/**
	 * Thread run method
	 */
	public void run() {
		while (true) {
			//System.out.println("write");
			try {
				// Process any pending changes
				//System.out.println(this.pendingChanges.size());
				synchronized (this.pendingChanges) {
					Iterator changes = this.pendingChanges.iterator();
					while (changes.hasNext()) {
						ChangeRequest change = (ChangeRequest) changes.next();
						switch (change.type) {
						case ChangeRequest.CHANGEOPS:
							SelectionKey key = change.socket.keyFor(this.selector);
							key.interestOps(change.ops);
							break;
						case ChangeRequest.REGISTER:
							change.socket.register(this.selector, change.ops);
							break;
						}
					}
					this.pendingChanges.clear();
				}

				this.selector.select();

				// Iterate over the set of keys for which events are available
				Iterator selectedKeys = this.selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = (SelectionKey) selectedKeys.next();
					selectedKeys.remove();

					if (!key.isValid()) {
						continue;
					}

					// Check what event is available and deal with it
					if (key.isConnectable()) {
						this.finishConnection(key);
					}
					if (key.isReadable()) {
						this.read(key);
					} else if (key.isWritable()) {
						this.write(key);
					}
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Read data from the socket connected to the server
	 * @param key
	 * @throws IOException
	 */
	private void read(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		// Clear out our read buffer so it's ready for new data
		this.readBuffer.clear();

		// Attempt to read off the channel
		int numRead;
		try {
			numRead = socketChannel.read(this.readBuffer);
		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel.
			key.cancel();
			socketChannel.close();
			return;
		}

		if (numRead == -1) {
			// Remote entity shut the socket down cleanly. Do the
			// same from our end and cancel the channel.
			key.channel().close();
			key.cancel();
			return;
		}
		
		// Hand the data off to our worker thread
		this.worker.processData(this, socketChannel, 
				this.readBuffer.array(), 
				numRead, 
				invoker);
	}

	/**
	 * Write data on the socket opened to the server
	 * @param key
	 * @throws IOException
	 */
	private void write(SelectionKey key) throws IOException {
		//SocketChannel socketChannel = (SocketChannel) key.channel();

		synchronized (this.pendingData) {
			List<ByteBuffer> queue = (List<ByteBuffer>) this.pendingData.get(socket);

			// Write until there's not more data ...
			if (queue != null) {
				while (!queue.isEmpty()) {
					ByteBuffer buf = (ByteBuffer) queue.get(0);
					socket.write(buf);
					if (buf.remaining() > 0) {
						// ... or the socket's buffer fills up
						break;
					}
					queue.remove(0);
				}

				//change to read mode
				if (queue.isEmpty()) {
					key.interestOps(SelectionKey.OP_READ);
				}
			}
		}
	}

	private void finishConnection(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();
	
		try {
			socketChannel.finishConnect();
		} catch (IOException e) {
			System.out.println(e);
			key.cancel();
			return;
		}
	
		key.interestOps(SelectionKey.OP_WRITE);
	}

	/**
	 * Set connection to ther server
	 * @return
	 * @throws IOException
	 */
	private SocketChannel initiateConnection() throws IOException {
		// Create a non-blocking socket channel
		SocketChannel socketChannel = SocketChannel.open();
		socketChannel.configureBlocking(false);

		socketChannel.connect(new InetSocketAddress(this.hostAddress, this.port));
	
		socketChannel.register(this.selector, SelectionKey.OP_CONNECT);
		
		
		return socketChannel;
	}

	/**
	 * Create a new selector
	 * @return
	 * @throws IOException
	 */
	private Selector initSelector() throws IOException {
		return SelectorProvider.provider().openSelector();
	}

	/**
	 * Get the logger associated to the class
	 * @return
	 */
	public Logger getLogger() {
		return this.logger;
	}

}