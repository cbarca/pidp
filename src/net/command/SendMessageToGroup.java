package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class SendMessageToGroup extends ACmdMessage {

	public SendMessageToGroup(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.sendMessageToGroup(args[0], 
									args[1], 
									args[2]);
		mediator.updateXML(args[0]);
	}

}
