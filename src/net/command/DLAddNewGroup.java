package net.command;

import gdl.DocumentList;
import gdl.DocumentListException;
import gui.model.User;

import java.io.IOException;
import java.net.MalformedURLException;

import mediator.Mediator;
import net.model.ECmdMessage;

import com.google.gdata.data.acl.AclRole;
import com.google.gdata.data.acl.AclScope;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.util.ServiceException;

public class DLAddNewGroup extends ACmdMessage {
	DocumentList docList;
	Mediator mediator;
	
	public DLAddNewGroup(ECmdMessage type, int numArgs, DocumentList docList, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.docList = docList;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
	
		try {
			User user = mediator.getUserByName(args[0]);
			DocumentListEntry docListEntry = docList.createNewDoc(args[1]);
			
			AclRole role = new AclRole("writer");
			AclScope scope = new AclScope(AclScope.Type.USER, user.getEmail());
			docList.addAclRole(role, scope, docListEntry);		
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
