package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class AddUserToGroup extends ACmdMessage {
		
	public AddUserToGroup(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.addUserToGroup(args[0], args[1], 
				Integer.parseInt(args[2]), 
				Integer.parseInt(args[3]), 
				Integer.parseInt(args[4]));
	}
}

