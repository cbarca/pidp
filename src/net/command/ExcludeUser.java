package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class ExcludeUser extends ACmdMessage {
		
	public ExcludeUser(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.excludeUser(args[0]);
	}
}

