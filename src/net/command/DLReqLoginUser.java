package net.command;

import java.io.IOException;
import java.net.MalformedURLException;

import com.google.gdata.data.acl.AclRole;
import com.google.gdata.data.acl.AclScope;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.util.ServiceException;

import gdl.DocumentList;
import gdl.DocumentListException;
import net.model.ECmdMessage;

public class DLReqLoginUser extends ACmdMessage {
	DocumentList docList;
	
	public DLReqLoginUser(ECmdMessage type, int numArgs, DocumentList docList) {
		this.type = type;
		this.numArgs = numArgs;
		this.docList = docList;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
	
		try {
			DocumentListEntry docListEntry = docList.createNewDoc(args[0]);
			
			AclRole role = new AclRole("writer");
			AclScope scope = new AclScope(AclScope.Type.USER, args[2]);
			docList.addAclRole(role, scope, docListEntry);		
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
