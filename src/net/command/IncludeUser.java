package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class IncludeUser extends ACmdMessage {
		
	public IncludeUser(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.includeUser(args[0], args[1], args[2]);
	}
}

