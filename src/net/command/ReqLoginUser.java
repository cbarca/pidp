package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class ReqLoginUser extends ACmdMessage {
	
	public ReqLoginUser(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		if (!mediator.verifUser(args[0], args[1])) {
			return;
		}
			
		mediator.includeUser(args[0], args[1], args[2]);
	}
}
