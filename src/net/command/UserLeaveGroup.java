package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class UserLeaveGroup extends ACmdMessage {
		
	public UserLeaveGroup(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.userLeaveGroup(args[0], args[1]);
	}
}

