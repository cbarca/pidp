package net.command;

import java.io.IOException;
import java.net.MalformedURLException;

import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.util.ServiceException;

import gdl.DocumentList;
import gdl.DocumentListException;
import gui.model.Group;
import gui.model.User;
import mediator.Mediator;
import net.model.ECmdMessage;

public class DLReqLogoutUser extends ACmdMessage{
	DocumentList docList;
	Mediator mediator;
	
	public DLReqLogoutUser(ECmdMessage type, int numArgs, DocumentList docList, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.docList = docList;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		User user = mediator.getUserByName(args[0]);
		
		for (Group group : user.getGroups()) {
			DocumentListEntry docListEntry = docList.getDocHash().get(group.getName());
						
			try {
								
				if (group.getUsers().size() == 1) {
					docList.deleteDoc(group.getName());
				}
				else {
					docList.removeAclRole("user", user.getEmail(), docListEntry);
				}
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocumentListException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
