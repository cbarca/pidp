package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class ReqLogoutUser extends ACmdMessage {

	public ReqLogoutUser(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
	}
}
