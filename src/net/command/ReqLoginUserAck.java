package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class ReqLoginUserAck extends ACmdMessage {
	
	public ReqLoginUserAck(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
	}
}
