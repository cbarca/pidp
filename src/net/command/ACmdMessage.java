package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;
import net.model.ICmdMessage;

public abstract class ACmdMessage implements ICmdMessage {
	ECmdMessage type;
	Mediator mediator;
	int numArgs;
	
	public abstract void execute(String[] args);
	
	@Override
	public boolean verifNumArgs(String[] args) {
		return args.length == numArgs;
	}
	
	@Override
	public String getName() {
		return type.name();
	}

}
