package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class AddNewGroup extends ACmdMessage {
		
	public AddNewGroup(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.addNewGroup(args[0], args[1]);
	}
}
