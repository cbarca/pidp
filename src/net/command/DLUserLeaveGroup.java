package net.command;

import gdl.DocumentList;
import gdl.DocumentListException;
import gui.model.Group;
import gui.model.User;

import java.io.IOException;
import java.net.MalformedURLException;

import mediator.Mediator;
import net.model.ECmdMessage;

import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.util.ServiceException;

public class DLUserLeaveGroup extends ACmdMessage {
	DocumentList docList;
	Mediator mediator;

	public DLUserLeaveGroup(ECmdMessage type, int numArgs, DocumentList docList, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.docList = docList;
		this.mediator = mediator;
	}

	@Override
	public void execute(String[] args) {

		if (!this.verifNumArgs(args)) {
			return;
		}

		try {
			User user = mediator.getUserByName(args[0]);
			Group group = mediator.getGroupByName(args[1]);
			DocumentListEntry docListEntry = docList.getDocHash().get(args[1]);

			if (group == null) {
				docList.deleteDoc(args[1]);
			}
			else {
				docList.removeAclRole("user", user.getEmail(), docListEntry);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
