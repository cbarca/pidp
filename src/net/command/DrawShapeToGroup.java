package net.command;

import mediator.Mediator;
import net.model.ECmdMessage;

public class DrawShapeToGroup extends ACmdMessage {

	public DrawShapeToGroup(ECmdMessage type, int numArgs, Mediator mediator) {
		this.type = type;
		this.numArgs = numArgs;
		this.mediator = mediator;
	}
	
	@Override
	public void execute(String[] args) {
		if (!this.verifNumArgs(args)) {
			return;
		}
		
		mediator.drawShapeToGroup(args[0], 
								  args[1], 
								  args[2], 
								  Integer.parseInt(args[3]), 
								  Integer.parseInt(args[4]));
		mediator.updateXML(args[0]);
	}
}
