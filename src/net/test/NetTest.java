package net.test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import net.Client;
import net.Server;
import net.controller.ClientWorker;
import net.controller.ServerWorker;
import junit.framework.TestCase;


public class NetTest extends TestCase {
	public NetTest(String name) {
		super(name);
	}
	
	/**
	 * Test connection works
	 */
	public void testCorrectConnection() {
		ServerWorker worker = new ServerWorker();
		Server server;
		try {
			server = new Server(InetAddress.getLocalHost(), 10000, worker);
			new Thread(worker).start();
			new Thread(server).start();
			assertTrue("Server up", true);
		} catch (UnknownHostException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
		
		ClientWorker cworker = new ClientWorker();
		Client client;
		try {
			client = new Client(InetAddress.getLocalHost(), 10000, cworker);
			cworker.setLogger(client.getLogger());
			Thread w = new Thread(cworker);
			w.setDaemon(true);
			w.start();
			Thread t = new Thread(client);
			t.setDaemon(true);
			t.start();
			//wait to have the connection set
			t.sleep(10);
			assertTrue("Client up", true);
		} catch (UnknownHostException e) {
			fail();
		} catch (IOException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}
	
	/**
	 * Test client and server communicate ok
	 */
	public void testExchangeMessages() {
		ServerWorker worker = new ServerWorker();
		Server server;
		try {
			server = new Server(InetAddress.getLocalHost(), 10003, worker);
			Thread ws = new Thread(worker);
			Thread s = new Thread(server);
			ws.start();
			s.start();
		
			ClientWorker cworker = new ClientWorker();
			Client client;
			try {
				client = new Client(InetAddress.getLocalHost(), 10003, cworker);
				cworker.setLogger(client.getLogger());
				Thread wc = new Thread(cworker);
				wc.setDaemon(true);
				wc.start();
				Thread c = new Thread(client);
				c.setDaemon(true);
				c.start();
				//wait to have the connection set
				c.sleep(10);
				
				//send message to server
				String message = "Hello server!";
				client.send(message.getBytes());
				
				ws.sleep(1000);
				
				//check message reached server
				if (worker.getLastMessage().equals(message))
					assertTrue("Message recevived ok", true);
				else
					fail();
				
			} catch (java.net.ConnectException e) {
				fail();
			} catch (java.nio.channels.CancelledKeyException e){
				fail();
			} catch (UnknownHostException e) {
				fail();
			} catch (IOException e) {
				fail();
			} catch (InterruptedException e) {
				fail();
			}
		} catch (UnknownHostException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
	
	
}
