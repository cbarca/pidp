package net;

import gdl.DocumentList;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;

import com.google.gdata.client.authn.oauth.OAuthException;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import mediator.Mediator;
import net.command.*;
import net.controller.*;
import net.model.ECmdMessage;

public class Server implements Runnable {
	// The host:port combination to listen on
	private InetAddress hostAddress;
	private int port;
	
	// Mediator
	private Mediator mediator;
	
	// Invoker
	private Invoker invoker;

	// The channel on which we'll accept connections
	private ServerSocketChannel serverChannel;

	// The selector we'll be monitoring
	private Selector selector;

	// The buffer into which we'll read data when it's available
	private ByteBuffer readBuffer = ByteBuffer.allocate(65536);

	// Worker 
	private ServerWorker worker;
	
	// A list of accepted sockets
	private List<SocketChannel> lSock = new ArrayList<SocketChannel>();

	// A list of PendingChange instances
	private List<ChangeRequest> pendingChanges = new LinkedList<ChangeRequest>();

	// Maps a SocketChannel to a list of ByteBuffer instances
	private Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<SocketChannel, List<ByteBuffer>>();
	
	// ServerCleaner
	private ServerCleaner cleaner;

	public Server(InetAddress hostAddress, int port, ServerWorker worker) throws IOException {
		this.hostAddress = hostAddress;
		this.port = port;
		this.selector = this.initSelector();
		this.worker = worker;
		
		// Get the mediator instance in Server mode
		mediator = Mediator.getInstance();
		
		invoker = new Invoker();
		
		// Store commands	
		invoker.storeCmd(new AddNewGroup(ECmdMessage.AddNewGroup, 2, mediator));
		invoker.storeCmd(new AddUserToGroup(ECmdMessage.AddUserToGroup, 5, mediator));
		invoker.storeCmd(new ReqLoginUser(ECmdMessage.ReqLoginUser, 3, mediator));
		invoker.storeCmd(new ReqLogoutUser(ECmdMessage.ReqLogoutUser, 1, mediator));
		invoker.storeCmd(new UserLeaveGroup(ECmdMessage.UserLeaveGroup, 2, mediator));
		invoker.storeCmd(new DrawShapeToGroup(ECmdMessage.DrawShapeToGroup, 5, mediator));
		invoker.storeCmd(new SendMessageToGroup(ECmdMessage.SendMessageToGroup, 3, mediator));
				
		// Start Document List
		DocumentList docList = DocumentList.getInstance();
		try {
			docList.getServerReqUrl();
			docList.getAccessToken();
			docList.loginService();
			
			// Make a hook to logout service on System.exit
			cleaner = new ServerCleaner(docList);
			
			// System.exit hook
			Thread hook = new Thread(cleaner);
		    Runtime.getRuntime().addShutdownHook(hook);
		    
		} catch (OAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Store commands (for Document List)
		invoker.storeCmd(new DLReqLoginUser(ECmdMessage.ReqLoginUser, 3, docList));
		invoker.storeCmd(new DLReqLogoutUser(ECmdMessage.ReqLogoutUser, 1, docList, mediator));
		invoker.storeCmd(new DLAddNewGroup(ECmdMessage.AddNewGroup, 2, docList, mediator));
		invoker.storeCmd(new DLAddUserToGroup(ECmdMessage.AddUserToGroup, 5, docList, mediator));
		invoker.storeCmd(new DLUserLeaveGroup(ECmdMessage.UserLeaveGroup, 2, docList, mediator));
	}

	public void send(SocketChannel socket, byte[] data) {
		synchronized (this.pendingChanges) {
			// Indicate we want the interest ops set changed
			this.pendingChanges.add(new ChangeRequest(socket, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

			// And queue the data we want written
			synchronized (this.pendingData) {
				List<ByteBuffer> queue = (List<ByteBuffer>) this.pendingData.get(socket);
				if (queue == null) {
					queue = new ArrayList<ByteBuffer>();
					this.pendingData.put(socket, queue);
				}
				queue.add(ByteBuffer.wrap(data));
			}
		}

		// Finally, wake up our selecting thread so it can make the required changes
		this.selector.wakeup();
	}

	public void run() {
		while (true) {
			try {
				// Process any pending changes
				synchronized (this.pendingChanges) {
					Iterator<ChangeRequest> changes = this.pendingChanges.iterator();
					while (changes.hasNext()) {
						ChangeRequest change = (ChangeRequest) changes.next();
						switch (change.type) {
						case ChangeRequest.CHANGEOPS:
							SelectionKey key = change.socket.keyFor(this.selector);
							if (key != null) {
								key.interestOps(change.ops);
							}
						}
					}
					this.pendingChanges.clear();
				}

				// Wait for an event one of the registered channels
				this.selector.select();				
				
				// Iterate over the set of keys for which events are available
				Iterator<SelectionKey> selectedKeys = this.selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = (SelectionKey) selectedKeys.next();
					selectedKeys.remove();

					if (!key.isValid()) {
						continue;
					}

					// Check what event is available and deal with it
					if (key.isAcceptable()) {
						this.accept(key);
					} else if (key.isReadable()) {
						this.read(key);
					} else if (key.isWritable()) {
						this.write(key);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void accept(SelectionKey key) throws IOException {
		// For an accept to be pending the channel must be a server socket channel.
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

		// Accept the connection and make it non-blocking
		SocketChannel socketChannel = serverSocketChannel.accept();
		socketChannel.configureBlocking(false);

		// Register the new SocketChannel with our Selector, indicating
		// we'd like to be notified when there's data waiting to be read
		socketChannel.register(this.selector, SelectionKey.OP_READ);
		
		System.out.println("ACCEPT");
		
		// Add the new channel
		lSock.add(socketChannel);
	}

	private void read(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		// Clear out our read buffer so it's ready for new data
		this.readBuffer.clear();

		// Attempt to read off the channel
		int numRead;
		try {
			numRead = socketChannel.read(this.readBuffer);
		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel.
			key.cancel();
			socketChannel.close();
			return;
		}

		if (numRead == -1) {
			// Remote entity shut the socket down cleanly. Do the
			// same from our end and cancel the channel.
			key.channel().close();
			key.cancel();
			lSock.remove(socketChannel);
			return;
		}

		// Hand the data off to our worker thread
		this.worker.processData(this, socketChannel, 
				this.readBuffer.array(), 
				numRead, 
				invoker,
				lSock);
	}

	private void write(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		synchronized (this.pendingData) {
			List<ByteBuffer> queue = (List<ByteBuffer>) this.pendingData.get(socketChannel);

			// Write until there's not more data ...
			while (!queue.isEmpty()) {
				ByteBuffer buf = (ByteBuffer) queue.get(0);
								
				socketChannel.write(buf);
				if (buf.remaining() > 0) {
					// ... or the socket's buffer fills up
					break;
				}
				queue.remove(0);
			}

			if (queue.isEmpty()) {
				// We wrote away all data, so we're no longer interested
				// in writing on this socket. Switch back to waiting for
				// data.
				key.interestOps(SelectionKey.OP_READ);
			}
		}
	}

	private Selector initSelector() throws IOException {
		// Create a new selector
		Selector socketSelector = SelectorProvider.provider().openSelector();

		// Create a new non-blocking server socket channel
		this.serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);

		// Bind the server socket to the specified address and port
		InetSocketAddress isa = new InetSocketAddress(this.hostAddress, this.port);
		serverChannel.socket().bind(isa);

		// Register the server socket channel, indicating an interest in 
		// accepting new connections
		serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);

		return socketSelector;
	}

	/**
	 * Server Cleaner class 
	 * Close connections after System.exit
	 * @author Cristi
	 *
	 */
	private static class ServerCleaner implements Runnable
	{
		DocumentList docList;
		
		private ServerCleaner(DocumentList docList)
		{
			this.docList = docList;
		}

		public void run()
		{
			try {
				docList.logoutService();
			} catch (OAuthException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}    
	}
	
	public static void main(String[] args) {
		try {
					
			ServerWorker worker = new ServerWorker();
			Server server = new Server(InetAddress.getLocalHost(), 10000, worker);
			
			new Thread(worker).start();
			new Thread(server).start();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
