package net.util;

import net.model.ECmdMessage;

public class GenerateCmdMessage {
	public static String generateCSVCmd(String who, ECmdMessage type, String[] args) {
		String strArgs = "";
		
		for (int i = 0; i < args.length - 1; i++) {
			strArgs += args[i] + ",";
		}
		strArgs += args[args.length - 1];
		
		return who + "," + type.name() + "," + strArgs + "\n";
	}
}
