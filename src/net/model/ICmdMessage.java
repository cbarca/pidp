package net.model;

public interface ICmdMessage {
	public String getName();
	public boolean verifNumArgs(String[] args);
	public void execute(String[] args);
}
