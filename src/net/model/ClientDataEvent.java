package net.model;
import java.nio.channels.SocketChannel;

import net.Client;

public class ClientDataEvent {
	public Client client;
	public SocketChannel socket;
	public byte[] data;
	
	public ClientDataEvent(Client client, SocketChannel socket, byte[] data) {
		this.client = client;
		this.socket = socket;
		this.data = data;
	}
}