package net.model;

public enum ECmdMessage {
	IncludeUser, ExcludeUser, LoginUser, 
	LogoutUser, UserLeaveGroup, AddNewGroup,
	AddUserToGroup, DrawShapeToGroup, SendMessageToGroup,
	ReqLoginUser, ReqLogoutUser, ReqLoginUserAck;
}
