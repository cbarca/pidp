package net.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;

import mediator.Mediator;
import net.Client;
import net.model.*;

import org.apache.log4j.Logger;

public class ClientWorker implements Runnable {
	private List<ClientDataEvent> queue = new LinkedList<ClientDataEvent>();
	private Invoker invoker;
	private Logger logger;
		
	public void processData(Client client, SocketChannel socket, byte[] data, int count, 
			Invoker invoker) {
		
		byte[] dataCopy = new byte[count];
		System.arraycopy(data, 0, dataCopy, 0, count);
		
		this.invoker = invoker;
		
		synchronized(queue) {
			queue.add(new ClientDataEvent(client, socket, dataCopy));
			queue.notify();
		}
	}
	
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	public void run() {
		ClientDataEvent dataEvent;
		
		while(true) {
			// Wait for data to become available
			synchronized(queue) {
				while(queue.isEmpty()) {
					try {
						queue.wait();
					} catch (InterruptedException e) {
					}
				}
				
				dataEvent = (ClientDataEvent) queue.remove(0);
			}
			
			//decode package from server
			byte[] input = dataEvent.data;
			byte[] data = new byte[input.length - 1];
			
			for (int i=1; i<input.length; i++)
				data[i-1] = input[i];
			
			//regular command
			if (input[0] == Byte.MIN_VALUE) {
				logger.debug("Read message:" + new String(data));
				invoker.executeCmd(new String(data));
			}
			//mediator
			else {
				logger.debug("Received mediator");
				
				// Deserialize from a byte array
				try {
					ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(data));
					Mediator globalMed = (Mediator) in.readObject();
					in.close();

					Mediator mediator = Mediator.getInstance();
					mediator.syncState(globalMed);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			
		}
	}
}
