package net.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.model.ICmdMessage;

/**
 * Invoker class (Command pattern)
 * @author Cristi
 *
 */
public class Invoker {
	List<ICmdMessage> cmds;	
	
	/**
	 * Constructor
	 */
	public Invoker() {
		cmds = new ArrayList<ICmdMessage>();
	}
	
	/**
	 * Stores a command
	 * @param cmd = command
	 */
	public void storeCmd(ICmdMessage cmd) {
		cmds.add(cmd);
	}
	
	/**
	 * Execute command given in message
	 * @param cmdMsg
	 * @return command in string form
	 */
	public void executeCmd(String cmdMsg) {
		cmdMsg = cmdMsg.replaceAll("\\r|\\n", "");
				
		String[] cmdElems = cmdMsg.split(",");
		
		if (cmdElems.length < 2) {
			return;
		}
		
		String who = cmdElems[0];
		String cmdName = cmdElems[1];
		String[] cmdArgs = Arrays.copyOfRange(cmdElems, 2, cmdElems.length);
		
		for (ICmdMessage cmd : cmds) {
			if (cmd.getName().compareTo(cmdName) == 0) {
				cmd.execute(cmdArgs);
			}
		}
	}
}
