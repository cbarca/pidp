package net.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.List;

import mediator.Mediator;
import net.Server;
import net.model.*;
import net.util.GenerateCmdMessage;


public class ServerWorker implements Runnable {
	private List<ServerDataEvent> queue = new LinkedList<ServerDataEvent>();
	private Invoker invoker;
	private List<SocketChannel> lSock;
	private List<String> messages = new LinkedList<String>();
		
	public void processData(Server server, SocketChannel socket, byte[] data, int count, 
			Invoker invoker,
			List<SocketChannel> lSock) {
		
		byte[] dataCopy = new byte[count];
		System.arraycopy(data, 0, dataCopy, 0, count);
		
		this.invoker = invoker;
		this.lSock = lSock;
		
		synchronized(queue) {
			queue.add(new ServerDataEvent(server, socket, dataCopy));
			messages.add(new String(dataCopy));
			queue.notify();
		}
	}
	
	public String getLastMessage() {
		return messages.get(messages.size() - 1);
	}
	/**
	 * Add type of the message at the beginning
	 * @param input
	 * @param type
	 * @return
	 */
	private byte[] packData(byte[] input, byte type) {
		byte[] pkg = new byte[input.length + 1];
		pkg[0] = type;
		
		for (int i=0; i<input.length; i++)
			pkg[i+1] = input[i];
		
		return pkg;
	}
	
	public void run() {
		ServerDataEvent dataEvent;
		
		while(true) {
			// Wait for data to become available
			synchronized(queue) {
				while(queue.isEmpty()) {
					try {
						queue.wait();
					} catch (InterruptedException e) {
					}
				}
				
				dataEvent = (ServerDataEvent) queue.remove(0);
			}
			
			String cmdMsg = new String(dataEvent.data);
			System.out.println("Received: " + cmdMsg);			
			invoker.executeCmd(cmdMsg);
			
			// Only users from list can access our server :)
			// Block all the other attemtps
			if (Mediator.getInstance().getUserByName(cmdMsg.split(",")[0]) == null) {
				continue;
			}
			
			/*
			 * ReqLogin command received
			 * 		send_to_source: 
			 * 			-> req_login_user_ack
			 * 			-> serialized mediator
			 * 			-> login_user
			 */
			
			// LOGIN
			if (cmdMsg.split(",")[1].compareTo(ECmdMessage.ReqLoginUser.name()) == 0) {
				String[] args = new String[1];
				args[0] = cmdMsg.split(",")[2];
				
			    try {
			    	// Serialize to a byte array
				    ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
				    ObjectOutput out = new ObjectOutputStream(bos) ;
					out.writeObject(Mediator.getInstance());
					out.close();
										
					// Send ReqLoginUserAck
				    String msg = GenerateCmdMessage.generateCSVCmd("Server", ECmdMessage.ReqLoginUserAck, args);
				    System.out.println("sending: " + msg);
				    byte[] msgPkg = packData(msg.getBytes(), Byte.MIN_VALUE);
				    dataEvent.server.send(dataEvent.socket, msgPkg);
				    
				    Thread.sleep(10);
				    
				    // Send mediator
				    System.out.println("sending: current_mediator's_state");
				    // Get the bytes of the serialized object
				    byte[] buf = bos.toByteArray();
				    byte[] medPkg = packData(buf, Byte.MAX_VALUE);
				    dataEvent.server.send(dataEvent.socket, medPkg);
				    
				    // Send serialized mediator
				    //dataEvent.server.send(dataEvent.socket, buf);
				    
				    Thread.sleep(10);
				    
				    // Send LoginUser
				    msg = GenerateCmdMessage.generateCSVCmd("Server", ECmdMessage.LoginUser, args);
				    System.out.println("sending: " + msg);
				    msgPkg = packData(msg.getBytes(), Byte.MIN_VALUE);
				    dataEvent.server.send(dataEvent.socket, msgPkg);
				    
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (SocketChannel socket : lSock) {
					// Pass to every other client an IncludeUser command message
					if (!socket.equals(dataEvent.socket)) {
						String msg = GenerateCmdMessage.generateCSVCmd("Server", ECmdMessage.IncludeUser, args);
						System.out.println("ToClient: " + msg);
					    byte[] msgPkg = packData(msg.getBytes(), Byte.MIN_VALUE);
					    
					    try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					    
					    dataEvent.server.send(socket, msgPkg);
					}
				}
			}
			else
			// LOGOUT
			if (cmdMsg.split(",")[1].compareTo(ECmdMessage.ReqLogoutUser.name()) == 0) {
				String[] args = new String[1];
				args[0] = cmdMsg.split(",")[2];

				for (SocketChannel socket : lSock) {
					// Pass to every other client an ExcludeUser command message
					if (!socket.equals(dataEvent.socket)) {
						String msg = GenerateCmdMessage.generateCSVCmd("Server", ECmdMessage.ExcludeUser, args);
						System.out.println("ToClient: " + msg);
					    byte[] msgPkg = packData(msg.getBytes(), Byte.MIN_VALUE);

					    try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						dataEvent.server.send(socket, msgPkg);
					}
				}
				
				Mediator.getInstance().excludeUser(args[0]);
				lSock.remove(dataEvent.socket);
			}
			else {
				// Normal routine
				for (SocketChannel socket : lSock) {
					// Pass the same command to the other clients
					if (!socket.equals(dataEvent.socket)) {
						System.out.println("ToClient: " + cmdMsg);
						byte[] msgPkg = packData(dataEvent.data, Byte.MIN_VALUE);
						
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					    dataEvent.server.send(socket, msgPkg);
					}
				}
			}
		}
	}
}
