package gui.view;

import java.awt.event.MouseAdapter;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.ListDataListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import gui.model.*;

public class GroupTreeView extends JScrollPane {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private JTree groupTree; 
	private ResponseListModel<Group> groups;
	private DefaultMutableTreeNode groupTreeRoot;
	private DefaultTreeModel groupTreeModel;
        
	/**
	 * Specialized constructor;
	 * @param groups = the list of groups, data model
	 */
	public GroupTreeView(ResponseListModel<Group> groups) {
		super();
		
		this.groups = groups;
		
		groupTreeRoot = new DefaultMutableTreeNode("All groups");
		groupTreeModel = new GroupTreeModel(groupTreeRoot);
		groupTree = new JTree(groupTreeModel);
		
		this.addGroupsTo(groupTreeRoot);
		this.setViewportView(groupTree);
		
		groups.addListDataListener((ListDataListener) groupTreeModel);
	}
	
	private void addGroupsTo(DefaultMutableTreeNode groupTreeRoot) {
		for (Group group : groups) {
			DefaultMutableTreeNode mgroup = new DefaultMutableTreeNode(group);
			for (User user : group.getUsers()) {
				DefaultMutableTreeNode muser = new DefaultMutableTreeNode(user);
				mgroup.add(muser);
			}
			groupTreeRoot.add(mgroup);
		}
	}
	
	public void addTreeMouseAdapter(MouseAdapter arg0) {
		groupTree.addMouseListener(arg0);
	}
}
