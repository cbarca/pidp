package gui.view;

import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextPane;

public class LoginView extends JPanel {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtfUsername, txtfEmail; 
	private JPasswordField txtfPassword;
	private JLabel lblUsername, lblPassword, lblEmail;
	private JButton btnLogin, btnReset;
	private JTextPane textpReqLink;

	/**
	 * Specialized constructor.
	 * Create the panel.
	 * @param users = the list of users, data model
	 */
	public LoginView(DefaultListModel users, String reqLink) {
		lblUsername = new JLabel("Username");
		lblPassword = new JLabel("Password");
		lblEmail = new JLabel("Gmail Account");
		
		txtfUsername = new JTextField();
		txtfUsername.setColumns(10);
		
		txtfEmail = new JTextField();
		txtfEmail.setColumns(10);
				
		txtfPassword = new JPasswordField();
		txtfPassword.setColumns(10);
		
		btnLogin = new JButton("Login");
		btnReset = new JButton("Reset");
		
		textpReqLink = new JTextPane();
		textpReqLink.setText("Grant access: \n" + reqLink);
						
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(113)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblEmail)
								.addComponent(lblUsername)
								.addComponent(lblPassword))
							.addGap(32)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtfEmail, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(txtfPassword)
									.addComponent(txtfUsername, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnReset)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnLogin))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(93)
							.addComponent(textpReqLink, GroupLayout.PREFERRED_SIZE, 311, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(97, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtfUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblUsername))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword)
						.addComponent(txtfPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblEmail)
						.addComponent(txtfEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLogin)
						.addComponent(btnReset))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textpReqLink, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		setLayout(groupLayout);
	}
	
	/**
	 * Login button listener
	 * @param loginListener = action listener object
	 */
	public void addLoginListener(ActionListener loginListener) {
		btnLogin.addActionListener(loginListener);
	}
	
	/**
	 * Reset button listener
	 * @param resetListener = action listener object
	 */
	public void addResetListener(ActionListener resetListener) {
		btnReset.addActionListener(resetListener);
	}
	
	/**
	 * Get username
	 * @return username (String)
	 */
	public String getUsername() {
		return txtfUsername.getText();
	}
	
	/**
	 * Reset username
	 */
	public void resetUsername() {
		txtfUsername.setText("");
	}
	
	/**
	 * Get password
	 * @return password (String)
	 */
	public String getPassword() {
		char[] pass = txtfPassword.getPassword();
		String passwd = "";
		
		for (char c : pass) {
			passwd += "" + c;
		}
		
		return passwd;
	}
	
	/**
	 * Get email
	 * @return email (String)
	 */
	public String getEmail() {
		return txtfEmail.getText();
	}
	
	/**
	 * Reset password. 
	 */
	public void resetPassword() {
		txtfPassword.setText("");
	}
}
