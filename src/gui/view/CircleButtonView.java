package gui.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.JToggleButton;

public class CircleButtonView extends JToggleButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6941726825535587638L;
	
	public CircleButtonView() {
		super(" ");
		setSize(new Dimension(35, 35));
		setBorderPainted(true);
		setMargin(new Insets(5, 12, 5, 12));
		setToolTipText("Draw circle");
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		int h = getHeight();
		g.setColor(Color.black);
		g.drawArc(2, 2, h - 4, h - 4, 0, 360);
	}

}
