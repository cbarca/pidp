package gui.view;

import java.awt.event.ActionListener;

import gui.model.*;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.DocumentListener;
import javax.swing.JButton;

public class GroupSelectorView extends JPanel {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtGroupName;
	private JLabel lblNewGroupName;
	private JButton btnOk;

	/**
	 * Create the panel.
	 */
	public GroupSelectorView(ResponseListModel<Group> groups) {
		
		lblNewGroupName = new JLabel("New group name");
		
		txtGroupName = new JTextField();
		txtGroupName.setText("");
		txtGroupName.setColumns(10);
		
		btnOk = new JButton("Ok");
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(86)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewGroupName)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(btnOk)
							.addComponent(txtGroupName, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(83, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(59)
					.addComponent(lblNewGroupName)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(txtGroupName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(26)
					.addComponent(btnOk)
					.addContainerGap(47, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
	
	public void addGroupNameListener(DocumentListener arg0) {
		txtGroupName.getDocument().addDocumentListener(arg0);
	}
	
	public void addBtnOkListener(ActionListener arg0) {
		btnOk.addActionListener(arg0);
	}
	
	public String getGroupName() {
		return txtGroupName.getText();
	}
}
