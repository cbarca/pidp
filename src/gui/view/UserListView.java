package gui.view;

import gui.model.*;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

public class UserListView extends JScrollPane {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JList userList;
	private ResponseListModel<User> uListModel; 
	
	/**
	 * Specialized constructor.
	 * @param users = the list of users, data model
	 */
	public UserListView(ResponseListModel<User> uListModel) {
		super();
		
		this.uListModel = uListModel;		
		
		userList = new JList(this.uListModel);
		userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		
		setViewportView(userList);
	}
	
	public void addUserListListener(ListSelectionListener arg0) {
		userList.addListSelectionListener(arg0);
	}
	
	public User getSelectedUser() {
		return (User)userList.getSelectedValue();
	}
}
