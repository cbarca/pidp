package gui.view;

import java.awt.event.ActionListener;

import gui.model.Group;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;

public class GrpTreeMenuView extends JPanel {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private Group group;
	private JPopupMenu popup;
	private JButton addUsrBtn;
	private JButton joinGrpBtn;
	private JButton leaveGrpBtn;

	/**
	 * Create the panel.
	 */
	public GrpTreeMenuView(Group group) {	
		this.popup = new JPopupMenu();
		this.group = group;
		
		addUsrBtn = new JButton("AddUser");
		
		joinGrpBtn = new JButton("JoinGroup");
		leaveGrpBtn = new JButton("LeaveGroup");
						
		this.popup.add(addUsrBtn);
		this.popup.add(joinGrpBtn);
		this.popup.add(leaveGrpBtn);	
	}
		
	public void show(JTree tree, int x, int y) {
		popup.show(tree, x, y);
	}
	
	public void hideBox() {
		popup.setVisible(false);
	}
	
	public Group getOnGroup() {
		return group;
	}
	
	public void setInvisibleJoin() {
		joinGrpBtn.setVisible(false);
		popup.validate();
	}
	
	public void setVisibleJoin() {
		joinGrpBtn.setVisible(true);
		popup.validate();
	}
	
	public void setInvisibleLeave() {
		leaveGrpBtn.setVisible(false);
		popup.validate();
	}
	
	public void setVisibleLeave() {
		leaveGrpBtn.setVisible(true);
		popup.validate();
	}
	
	public void addJoinGrpListener(ActionListener arg0) {
		joinGrpBtn.addActionListener(arg0);
	}
	
	public void addLeaveGrpListener(ActionListener arg0) {
		leaveGrpBtn.addActionListener(arg0);
	}
	
	public void addAddUsrListener(ActionListener arg0) {
		addUsrBtn.addActionListener(arg0);
	}
}
