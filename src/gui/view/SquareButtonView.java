package gui.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JToggleButton;

public class SquareButtonView extends JToggleButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SquareButtonView() {
		super(" ");
		setSize(new Dimension(35, 35));
		setBorderPainted(true);
		setMargin(new Insets(5, 12, 5, 12));
		setToolTipText("Draw square");
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		int h = getHeight();
		int w = getWidth();
		g.setColor(Color.black);
		g.drawRect(4, 4, w - 8, h - 8);
	}

}
