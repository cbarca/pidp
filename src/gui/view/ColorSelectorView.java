package gui.view;

import java.awt.event.ActionListener;

import gui.model.*;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.ListCellRenderer;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

public class ColorSelectorView extends JPanel {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblSelectColor, lblUser, lblUsername;
	private JComboBox selColorBox;
	private JButton btnSelect;
	private Group group;
	private User user;
	private List<Color> grpCol;
	
	class ColorCellRenderer implements ListCellRenderer {
		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

		// width doesn't matter as combobox will size
		private Dimension preferredSize = new Dimension(200, 20);

		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			JLabel renderer = (JLabel) defaultRenderer
			.getListCellRendererComponent(list, value, index,
					isSelected, cellHasFocus);
			if (value instanceof Color) {
				renderer.setBackground((Color) value);
			}
			renderer.setPreferredSize(preferredSize);
			return renderer;
		}
	}
	
	/**
	 * Create the panel.
	 */
	public ColorSelectorView(Group group, User user) {
		this.group = group;
		this.user = user;
		
		grpCol = group.getAvailColors();
		
		lblSelectColor = new JLabel("Color Palette");
		selColorBox = new JComboBox();
		selColorBox.setRenderer(new ColorCellRenderer());
		
		for (Color col : grpCol) {
			selColorBox.addItem(col);
		}
		
		selColorBox.validate();
		selColorBox.setSelectedIndex(0);
		
		btnSelect = new JButton("Select");
		btnSelect.requestFocusInWindow();
		
		lblUser = new JLabel("User:");
		lblUsername = new JLabel(user.getName());
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(51)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblUser)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE))
						.addComponent(selColorBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSelect)
						.addComponent(lblSelectColor, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(50, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUser)
						.addComponent(lblUsername))
					.addGap(12)
					.addComponent(lblSelectColor)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(selColorBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(13)
					.addComponent(btnSelect)
					.addContainerGap(32, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
	
	public void addSelectBtnListener(ActionListener arg0) {
		btnSelect.addActionListener(arg0);
	}
	
	public void addSelectBoxListener(ActionListener arg0) {
		selColorBox.addActionListener(arg0);
	}
	
	public Color getSelectedColor() {
		return (Color)selColorBox.getSelectedItem();
	}
	
	public Group getGroup() {
		return group;
	}
	
	public User getUser() {
		return user;
	}
}
