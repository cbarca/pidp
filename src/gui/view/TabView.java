/*
 * TabView.java
 *
 * Created on Mar 16, 2012, 5:57:56 PM
 */

package gui.view;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import gui.util.*;
import gui.drawing.*;
import gui.model.*;

/**
 *
 * @author ana
 */
public class TabView extends javax.swing.JPanel {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** Creates new form TestView */
    public TabView(Group group) {
    	this.group = group;
        initComponents();
        addOns();
    }

    private void initComponents() {
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        //jList1 = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        squareButton = new SquareButtonView();
        circleButton = new CircleButtonView();
        lineButton = new LineButtonView();
        arrowButton = new ArrowButtonView();
        

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jScrollPane3.setViewportView(jTextArea2);

        setMaximumSize(new java.awt.Dimension(500, 450));
        setPreferredSize(new java.awt.Dimension(500, 450));

        jTextPane1.setSize(20, 5);
        jTextPane1.setEditable(false);
        jScrollPane1.setViewportView(jTextPane1);

        jList1 = new JList();
        
        ResponseListModel<User> users = group.getUsers();
        jList1.setModel(new javax.swing.AbstractListModel() {
	        ResponseListModel<User> users = group.getUsers();
	        public int getSize() { return users.getSize(); }
	        public Object getElementAt(int i) { return users.get(i).getName(); }
        });
        jList1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jList1.setCellRenderer(new MyRenderer(group));
        
        jScrollPane2.setViewportView(jList1);

        jLabel1.setText(" Font");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("Color");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jScrollPane4.setViewportView(jTextArea3);

        jButton1.setText("Send");

        jToolBar1.setRollover(true);

        jLabel3.setText("Legenda");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 437, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 177, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jToolBar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(365, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jLabel2)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 374, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 94, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(8, 8, 8))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel3))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jToolBar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 32, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(27, 27, 27)
                        .add(jLabel3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 75, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(jButton1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE))
                .add(20, 20, 20))
        );
        
        restoreHistory(group);
    }// </editor-fold>


    /**
     * Update view
     */
    private void addOns() {
    	
    	//drawing buttons
    	squareButton.setName("Square");
    	circleButton.setName("Circle");
    	lineButton.setName("Line");
    	arrowButton.setName("Arrow");
        jToolBar1.add(squareButton);
        jToolBar1.add(circleButton);
        jToolBar1.add(lineButton);
        jToolBar1.add(arrowButton);
        
        //combobox options
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Red", "Green", "Blue", "Black" }));
        
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { Font.SERIF, Font.SANS_SERIF, 
        		Font.DIALOG_INPUT, Font.MONOSPACED }));
    }
    
    
    /**
     * Text color listener
     * @param colorListener
     */
    public void addColorListener(ActionListener colorListener) {
    	jComboBox2.addActionListener(colorListener);
    }
    
    /**
     * Text font listener
     * @param fontListener
     */
    public void addFontListener(ActionListener fontListener) {
    	jComboBox1.addActionListener(fontListener);
    }
    
    /**
     * "Send" button listener
     * @param messageListener
     */
    public void addMessageListener(ActionListener messageListener) {
    	jButton1.addActionListener(messageListener);
    }
    
    /**
     * Shape selection listener
     * @param drawingButtonListener
     */
    public void addDrawingButtonListener(ActionListener drawingButtonListener) {
    	squareButton.addActionListener(drawingButtonListener);
    	circleButton.addActionListener(drawingButtonListener);
    	lineButton.addActionListener(drawingButtonListener);
    	arrowButton.addActionListener(drawingButtonListener);
    }
    
    /**
     * Canvas listener
     * @param canvasListener
     */
    public void addCanvasListsener(MouseListener canvasListener) {
    	jPanel1.addMouseListener(canvasListener);
    }
    
    public void addRedrawListener(KeyListener keyListener) {
    	this.addKeyListener(keyListener);
    }
    
    /**
     * Read message from input area
     * @return
     */
    public String getMessageText() {
    	return jTextArea3.getText();
    }
    
    /**
     * Clear input area text
     */
    public void clearText() {
    	jTextArea3.setText("");
    }
    
    
    /**
     * Display all the messages in the tab's list
     * @param messages
     */
    public void updateDisplayedMessages(List<MessageInfo> messages) {
    	String messagesList = "";
    	SimpleAttributeSet[] styles = new SimpleAttributeSet[messages.size()];
    	ArrayList<Pair<Integer, Integer>> textIndexes = new ArrayList<Pair<Integer, Integer>>();
        int start = 0;
        
    	for (int i=0; i<messages.size(); i++) {
    		String message = "";
    		MessageInfo currentMsg = messages.get(i);
    	    
    		styles[i] = new SimpleAttributeSet();
    		StyleConstants.setForeground(styles[i], currentMsg.getColor());
            StyleConstants.setFontFamily(styles[i], currentMsg.getFont());
            
    		message += currentMsg.getUser().getName() + " : ";
    		message += currentMsg.getMessage() + "\n";
    		messagesList += message;
    		textIndexes.add(new Pair<Integer, Integer>(start, message.length()));
    		start += message.length();
    	}
    	
    	jTextPane1.setText(messagesList);
        StyledDocument sdoc = jTextPane1.getStyledDocument();
        
        for (int i=0; i<messages.size(); i++) {
	        sdoc.setCharacterAttributes(textIndexes.get(i).fst, textIndexes.get(i).snd, styles[i], false);
        }
    }
    
    /**
     * Single selection for the drawing buttons
     * @param selectedButton
     */
    public void deselectButtons(String selectedButton) {
    	circleButton.setSelected(false);
		arrowButton.setSelected(false);
		lineButton.setSelected(false);
		squareButton.setSelected(false);
		if (selectedButton.equals("Square")) {
    		squareButton.setSelected(true);
    	}
		if (selectedButton.equals("Circle")) {
    		circleButton.setSelected(true);
    	}
		if (selectedButton.equals("Line")) {
    		lineButton.setSelected(true);
    	}
		if (selectedButton.equals("Arrow")) {
    		arrowButton.setSelected(true);
    	}
    }
    
    /**
     * Draw shape selected by user
     * @param shape
     * @param x
     * @param y
     */
    public void draw(List<Drawing> drawings) {
    	Graphics g = jPanel1.getGraphics();
    	for (int i=0; i<drawings.size(); i++)
    		drawings.get(i).draw(g);
    }
    
    /**
     * Redraw shapes and reprint text
     * @param group
     */
    public void restoreHistory(Group group) {
    	draw(group.getDrawings());
    	jList1.updateUI();
    	updateDisplayedMessages(group.getLoggedMessages());
    }
	
    /**
     * Users list formatting
     * @author ana
     *
     */
    public class MyRenderer extends JLabel implements ListCellRenderer {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Group group;
    	
        public MyRenderer(Group group) {
            setOpaque(true);
            this.group = group;
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            setText(value.toString());
            setBackground(group.getUserColor(group.getUsers().get(index)));

            return this;
        }
    }
    
    /**
     * Get the name of the group that corresponds to the tab
     * @return
     */
    public String getGroupName() {
    	return group.getName();
    }
    
    // Variables declaration - do not modify
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    public javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JToolBar jToolBar1;
    private SquareButtonView squareButton;
    private CircleButtonView circleButton;
    private LineButtonView lineButton;
    private ArrowButtonView arrowButton;
    // End of variables declaration
    
    private Group group;

}
