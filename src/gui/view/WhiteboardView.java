package gui.view;

import java.awt.FlowLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import gui.model.*;
import mediator.*;
import gui.controller.*;

public class WhiteboardView extends JPanel {
	/**
	 * private members
	 */
	private static final long serialVersionUID = 1L;
	private ResponseListModel<Group> groups;
	private JTabbedPane tabbedPane;
	private Mediator mediator;

	/**
	 * Create the panel.
	 */
	public WhiteboardView(final ResponseListModel<Group> groups, Mediator mediator) {
		
		super();
		
		this.groups = groups;
		this.mediator = mediator;
		
		setLayout(new FlowLayout());
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		
		
		//group tabs
		for (int i=0; i<groups.size(); i++) {
			TabView tabView = new TabView(groups.get(i));
			TabController tabController = new TabController(mediator, tabView, groups.get(i));
			tabbedPane.addTab(groups.get(i).getName(), tabView);
		}
		
		tabbedPane.addChangeListener(new ChangeListener() {
		    // This method is called whenever the selected tab changes
		    public void stateChanged(ChangeEvent evt) {
	        	TabView tab = (TabView)tabbedPane.getSelectedComponent();
	        	
	        	if (tab != null) {
	        		tab.updateUI();
	        	}
		    }
		});
		
		add(tabbedPane);
		
	}
	
	/**
	 * Get the associated JTabbedPane
	 * @return
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

}
