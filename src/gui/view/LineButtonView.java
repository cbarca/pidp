package gui.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.JToggleButton;

public class LineButtonView extends JToggleButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6941726825535587638L;
	
	public LineButtonView() {
		super(" ");
		setSize(new Dimension(35, 35));
		setBorderPainted(true);
		setMargin(new Insets(5, 12, 5, 12));
		setToolTipText("Draw line");
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		int h = getHeight();
		g.setColor(Color.black);
		g.drawLine(2, 2, h-2, h-2);
	}

}
