package gui.view;

import java.awt.event.ActionListener;

import gui.model.*;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JProgressBar;

public class CommandBarView extends JPanel {
	/**
	 * Private members;
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblLoggedAs, lblCurrUsername;
	private JButton btnLogout;
	private JButton btnSaveGroupWork;
	private JButton btnCreateGroup;
	private JProgressBar progressBar;
	private JLabel lblSavingProgress;

	/**
	 * Create the panel.
	 * Specialized constructor.
	 * @param currUser = current user object, data model
	 */
	public CommandBarView(User currUser) {
		lblLoggedAs = new JLabel("LoggedAs:");
		lblCurrUsername = new JLabel("username");
		this.setCurrentUsername(currUser.getName());
		
		btnLogout = new JButton("Logout");
		btnSaveGroupWork = new JButton("Save group work");
		btnCreateGroup = new JButton("Create group");
		
		progressBar = new JProgressBar();
		lblSavingProgress = new JLabel("Saving progress...");
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblLoggedAs)
									.addGap(10)
									.addComponent(lblCurrUsername, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnSaveGroupWork)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnCreateGroup)))
							.addGap(60)
							.addComponent(btnLogout))
						.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
						.addComponent(lblSavingProgress))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCurrUsername)
						.addComponent(lblLoggedAs))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSaveGroupWork)
						.addComponent(btnCreateGroup)
						.addComponent(btnLogout))
					.addGap(38)
					.addComponent(lblSavingProgress)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}
	
	/**
	 * Set the text inside the textLabel with username
	 * @param name = current username
	 */
	public void setCurrentUsername(String name) {
		lblCurrUsername.setText(name);
	}
	
	/**
	 * Save group work button listener
	 * @param saveListener = action listener object
	 */
	public void addSaveGoupWorkListener(ActionListener saveListener) {
		btnSaveGroupWork.addActionListener(saveListener);
	}
	
	/**
	 * Logout button listener
	 * @param logoutListener = action listener object
	 */
	public void addLogoutListener(ActionListener logoutListener) {
		btnLogout.addActionListener(logoutListener);
	}
	
	/**
	 * Create group button listener
	 * @param createGroupListener = action listener object
	 */
	public void addCreateGroupListener(ActionListener createGroupListener) {
		btnCreateGroup.addActionListener(createGroupListener);
	}
	
	/**
	 * Set progressbar value (progress rate)
	 * @param vol = progress rate
	 */
	public void setSavingProgress(int vol) {
		progressBar.setValue(vol);
	}
	
	/**
	 * Disable the save button
	 * @param ok = boolean state 
	 */
	public void disableSaveBtn(boolean ok) {
		btnSaveGroupWork.setEnabled(!ok);
	}
}
