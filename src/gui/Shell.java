package gui;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import mediator.*;
import gui.controller.*;
import gui.view.*;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Shell extends JPanel {
	/**
	 * Private members
	 */
	
	// Suid
	private static final long serialVersionUID = 1L;
	
	// Mediator
	private Mediator mediator;
	
	// Regions
	private JPanel topRegion, topLeftRegion, topRightRegion,
				   bottomRegion, bottomLeftRegion, bottomRightRegion;
	
	// UserList
	private UserListController ulController;
	private UserListView ulView;
	
	// CommandBar
	private CommandBarController cmdBarController;
	private CommandBarView cmdBarView;
	
	// GroupTree
	private GroupTreeController grpTreeController;
	private GroupTreeView grpTreeView;
	
	//Whiteboard
	private WhiteboardController whiteboardController;
	private WhiteboardView whiteboardView;
	
	/**
	 * Specialized constructor. 
	 * Create the panel.
	 * @param mediator = mediator object
	 */
	public Shell(Mediator mediator) {
		this.mediator = mediator;
		
		setLayout(new BorderLayout());
		
		// Init regions
		initTopRegion();
		initBottomRegion();
		
		// Init components
		initUserList();
		initCommandBar();
		initGroupTree();
		initWhiteboard();
	}	
	
	/**
	 * Init top region = topLeft + topRight
	 */
	private void initTopRegion() {
		topRegion = new JPanel();
		add(topRegion, BorderLayout.NORTH);
		GridBagLayout gbl_topRegion = new GridBagLayout();
		gbl_topRegion.columnWidths = new int[]{105, 505, 0};
		gbl_topRegion.rowHeights = new int[]{155, 0};
		gbl_topRegion.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_topRegion.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		topRegion.setLayout(gbl_topRegion);
		
		topLeftRegion = new JPanel();
		FlowLayout flowLayout = (FlowLayout) topLeftRegion.getLayout();
		flowLayout.setVgap(1);
		flowLayout.setHgap(1);
		flowLayout.setAlignment(FlowLayout.LEFT);
		topLeftRegion.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_topLeftRegion = new GridBagConstraints();
		gbc_topLeftRegion.anchor = GridBagConstraints.NORTHWEST;
		gbc_topLeftRegion.fill = GridBagConstraints.NONE;
		gbc_topLeftRegion.insets = new Insets(0, 0, 0, 0);
		gbc_topLeftRegion.gridx = 0;
		gbc_topLeftRegion.gridy = 0;
		topLeftRegion.setPreferredSize(new Dimension(105,155));
		topRegion.add(topLeftRegion, gbc_topLeftRegion);
		
		topRightRegion = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) topRightRegion.getLayout();
		flowLayout_1.setVgap(1);
		flowLayout_1.setHgap(1);
		topRightRegion.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_topRightRegion = new GridBagConstraints();
		gbc_topRightRegion.fill = GridBagConstraints.BOTH;
		gbc_topRightRegion.gridx = 1;
		gbc_topRightRegion.gridy = 0;
		topRightRegion.setPreferredSize(new Dimension(505,155));
		topRegion.add(topRightRegion, gbc_topRightRegion);	
	}
	
	/**
	 * Init bottom region = bottomLeft + bottomRight
	 */
	private void initBottomRegion() {
		bottomRegion = new JPanel();
		add(bottomRegion, BorderLayout.CENTER);
		GridBagLayout gbl_bottomRegion = new GridBagLayout();
		gbl_bottomRegion.columnWidths = new int[]{105, 505, 0};
		gbl_bottomRegion.rowHeights = new int[]{455, 0};
		gbl_bottomRegion.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_bottomRegion.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		bottomRegion.setLayout(gbl_bottomRegion);
		
		bottomLeftRegion = new JPanel();
		FlowLayout flowLayout = (FlowLayout) bottomLeftRegion.getLayout();
		flowLayout.setVgap(1);
		flowLayout.setHgap(1);
		flowLayout.setAlignment(FlowLayout.LEFT);
		bottomLeftRegion.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_bottomLeftRegion = new GridBagConstraints();
		gbc_bottomLeftRegion.anchor = GridBagConstraints.WEST;
		gbc_bottomLeftRegion.fill = GridBagConstraints.VERTICAL;
		gbc_bottomLeftRegion.insets = new Insets(0, 0, 0, 0);
		gbc_bottomLeftRegion.gridx = 0;
		gbc_bottomLeftRegion.gridy = 0;
		bottomLeftRegion.setPreferredSize(new Dimension(105,455));
		bottomRegion.add(bottomLeftRegion, gbc_bottomLeftRegion);
				
		bottomRightRegion = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) bottomRightRegion.getLayout();
		flowLayout_1.setHgap(1);
		flowLayout_1.setVgap(1);
		bottomRightRegion.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_bottomRightRegion = new GridBagConstraints();
		gbc_bottomRightRegion.gridx = 1;
		gbc_bottomRightRegion.gridy = 0;
		bottomRightRegion.setPreferredSize(new Dimension(505,455));
		bottomRegion.add(bottomRightRegion, gbc_bottomRightRegion);
	}
	
	/**
	 * Init the user list from the left corner
	 */
	private void initUserList() {
		ulView = new UserListView(mediator.getUserList());
		ulController = new UserListController(mediator, ulView);
		
		ulView.setPreferredSize(new Dimension(100,150));
		
		topLeftRegion.add(ulView);
	}
	
	/**
	 * Init the command bar from the right corner
	 */
	private void initCommandBar() {
		cmdBarView = new CommandBarView(mediator.getCurrentUser());
		cmdBarController = new CommandBarController(mediator, cmdBarView);
		
		topRightRegion.add(cmdBarView);
	}
	
	/**
	 * Init the group tree from the left side
	 */
	private void initGroupTree() {
		grpTreeView = new GroupTreeView(mediator.getGroupList());
		grpTreeController = new GroupTreeController(mediator, grpTreeView);
		
		grpTreeView.setPreferredSize(new Dimension(100,450));
		
		bottomLeftRegion.add(grpTreeView);
	}
	
	/**
	 * Init the whiteboard corner
	 */
	private void initWhiteboard() {
		whiteboardView = new WhiteboardView(mediator.getCurrentUser().getGroups(), mediator);
		whiteboardController = new WhiteboardController(mediator.getCurrentUser().getGroups(), whiteboardView, mediator);
		whiteboardView.setPreferredSize(new Dimension(500, 450));
		mediator.addWhiteboardController(whiteboardController);
		
		bottomRightRegion.add(whiteboardView);
		cmdBarController.setWhiteboardController(whiteboardController);
		grpTreeController.setWhiteboardController(whiteboardController);
	}
}
