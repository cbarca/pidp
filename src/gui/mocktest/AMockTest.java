package gui.mocktest;

import javax.swing.SwingWorker;

public abstract class AMockTest extends SwingWorker<Object, Object> {
	
	public abstract boolean test();
}
