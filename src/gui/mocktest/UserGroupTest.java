package gui.mocktest;

import mediator.Mediator;

public class UserGroupTest extends AMockTest {
	Mediator mediator;
	
	public UserGroupTest(Mediator mediator) {
		this.mediator = mediator;
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		test();
		
		return null;
	}

	@Override
	public boolean test() {	
		try {
			testLoginLogout();
			testDrawings();
			testMessages();
			testInOutGroups();			
			testAddUserGroup();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}
	
	private void testLoginLogout() throws InterruptedException {
		Thread.sleep(1000);
		mediator.includeUser("u2", "2", "bb@g");
		Thread.sleep(1000);
		mediator.includeUser("u3", "3", "cc@g");
		Thread.sleep(1000);
		mediator.includeUser("u4", "4", "dd@g");
		Thread.sleep(1000);
		mediator.includeUser("u5", "5", "ee@g");
		publish(mediator.getUserList().size());
		Thread.sleep(1500);	
		mediator.excludeUser("u2");
		Thread.sleep(1500);
		mediator.excludeUser("u3");
		publish(mediator.getUserList().size());
	}
	
	private void testInOutGroups() throws InterruptedException {
		Thread.sleep(1000);
		mediator.includeUser("uInOut", "6", "ff@g");
		Thread.sleep(1000);
		mediator.addUserToGroup("uInOut", "u1", 255, 31, 0);
		Thread.sleep(1000);
		mediator.addUserToGroup("uInOut", "u4", 255, 31, 0);
		Thread.sleep(1000);
		mediator.addUserToGroup("uInOut", "u5", 255, 31, 0);
		Thread.sleep(1000);
		mediator.userLeaveGroup("uInOut", "u1");
	}
	
	private void testAddUserGroup() throws InterruptedException {
		Thread.sleep(1000);
		mediator.includeUser("uAddUG", "7", "gg@g");
		Thread.sleep(1000);
		mediator.addNewGroup("uAddUG", "G1");
		Thread.sleep(1000);
		mediator.addNewGroup("uAddUG", "G2");
		Thread.sleep(1000);
		mediator.addUserToGroup("uInOut", "G1", 255, 31, 0);
		Thread.sleep(1000);
		mediator.addUserToGroup("u1", "G2", 255, 61, 0);
		Thread.sleep(1000);
		mediator.addUserToGroup("u4", "G2", 255, 92, 0);
		Thread.sleep(1000);
		mediator.addUserToGroup("u5", "G2", 255, 122, 0);
		Thread.sleep(1000);
		mediator.userLeaveGroup("u4", "G2");
		Thread.sleep(1000);
		mediator.excludeUser("u5");
	}
	
	private void testDrawings() throws InterruptedException {
		Thread.sleep(1000);
		mediator.drawShapeToGroup("u1", "u1", "Circle", 80, 80);
		Thread.sleep(1000);
		mediator.drawShapeToGroup("u1", "u1", "Square", 120, 120);
		Thread.sleep(1000);
		mediator.drawShapeToGroup("u1", "u1", "Line", 150, 70);
		Thread.sleep(1000);
		mediator.drawShapeToGroup("u1", "u1", "Arrow", 300, 60);
	}
	
	private void testMessages() throws InterruptedException {
		Thread.sleep(1000);
		mediator.sendMessageToGroup("u1", "u1", "ana are mere?");
		Thread.sleep(1000);
		mediator.sendMessageToGroup("u1", "u1", "ana are pere");
	}
	
	@Override
	protected void done() {
		if (isCancelled())
			System.out.println("Cancelled !");
		else {
			System.out.println("Done !");
			setProgress(0);
		}	
	}
}
