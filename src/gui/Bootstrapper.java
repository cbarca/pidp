package gui;

import mediator.*;
import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;
import gdl.DocumentList;
import gui.controller.*;
import gui.mocktest.AMockTest;
import gui.mocktest.UserGroupTest;
import gui.view.*;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import com.google.gdata.client.authn.oauth.OAuthException;

public class Bootstrapper {

	private JFrame bootFrame;
	private Mediator mediator;
	private DocumentList docList;
	private AMockTest aMockTest;
	private String username = null;

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bootstrapper window;
					if (args.length > 0)
						window = new Bootstrapper(args[0]);
					else
						window = new Bootstrapper();
					window.bootFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application(providing username).
	 */
	public Bootstrapper(String username) {
		this.username = username;
		initialize();
	}

	/**
	 * Create the application.
	 */
	public Bootstrapper() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		bootFrame = new JFrame("CollabChat");	
		bootFrame.setSize(new Dimension(620, 650));
		bootFrame.setMinimumSize(new Dimension(620, 650));

		docList = DocumentList.getInstance();
		
		bootFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 	
		mediator = Mediator.getInstance(this, this.username);
		bootFrame.addWindowListener(new CloseBootstrapper());
		
		//aMockTest = new UserGroupTest(mediator);
		//aMockTest.execute();
						
		this.BootLogin();
	}
		
	/**
	 * Login page
	 */
	public void BootLogin() {
		// Set LoginView + LoginController
		LoginView loginView = new LoginView(mediator.getUserList(), docList.getClientReqUrl());
		LoginController loginController = new LoginController(mediator, loginView);
		// Init bootFrame with LoginView
		bootFrame.setContentPane(loginView);
		bootFrame.validate();
	}
	
	/**
	 * Change set content pane 
	 * @param cont = container
	 */
	public void BootTo(Container cont) {
		bootFrame.setContentPane(cont);
		bootFrame.validate();
	}
}

class CloseBootstrapper implements WindowListener {

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e){    	
    	System.exit(0);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		Mediator mediator = Mediator.getInstance();
		DocumentList docList = DocumentList.getInstance();
		
		if (mediator.getCurrentUser() != null) {
			String[] args = new String[1];
			args[0] = mediator.getCurrentUser().getName();
			mediator.getClient().send(
					GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
    											ECmdMessage.ReqLogoutUser, 
    											args).getBytes());
			try {
				docList.logoutService();
			} catch (OAuthException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
    	
    	System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
