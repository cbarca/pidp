package gui.drawing;

import java.awt.Color;
import java.awt.Graphics;

public class Line extends Drawing {

	public Line(int xpt, int ypt, Color c) {
		x = xpt;
		y = ypt;
		w = 40;
		h = 30;
		color = c;
		shape = "line";
		saveAsRect();
	}

	public void draw(Graphics g) {
		g.setColor(color);
		g.drawLine(x, y, x+w, y+h);
	}
}
