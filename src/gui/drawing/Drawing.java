package gui.drawing;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Drawing {
	protected int x, y, w, h;
	protected Color color;
	protected String shape;

	protected Rectangle rect;

	public void draw(Graphics g) {
	}

	public void move(int xpt, int ypt) {
		x = xpt;
		y = ypt;
	}

	public boolean contains(int x, int y) {
		return rect.contains(x, y);
	}

	protected void saveAsRect() {
		rect = new Rectangle(x - w / 2, y - h / 2, w, h);
	}
	
	public void setShape(String shape) {
		this.shape = shape;
	}
	
	public String toString() {
		String s = "";
		s += shape + " x:" + x + " y:" + y + " color:" + color.toString();
		return s;
	}

}