package gui.drawing;

import java.awt.Color;
import java.awt.Graphics;


public class Square extends Drawing {

	public Square(int xpt, int ypt, Color c) {
		x = xpt;
		y = ypt;
		w = 40;
		h = 30;
		color = c;
		shape = "square";
		saveAsRect();
	}

	public void draw(Graphics g) {
		g.setColor(color);
		g.drawRect(x, y, w, h);
	}
}