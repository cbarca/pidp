package gui.model;

import java.io.Serializable;

/**
 * 
 * @author Cristi
 *
 */
public class User implements Serializable {
	/**
	 * Constants
	 */
	public static final String DEFAULT_NAME = "JohnDoe", 
		DEFAULT_PASSWD = "123", DEFAULT_EMAIL = "JohnDoe@gmail.com";
	
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private String name, password, email;
	private ResponseListModel<Group> groups;
	
	/**
	 * Default constructor
	 */
	public User() {
		this(DEFAULT_NAME);
	}
	
	/**
	 * Specialized constructor.
	 * @param name = user name
	 */
	public User(String name) {
		this(name, DEFAULT_PASSWD);
	}
	
	/**
	 * Specialized constructor
	 * @param name = user name
	 * @param password = user password
	 */
	public User(String name, String password) {
		this(name, password, DEFAULT_EMAIL);
	}
	
	/**
	 * Specialized constructor
	 * @param name = user name
	 * @param password = user password
	 * @param email = user email
	 */
	public User(String name, String password, String email) {
		this.setName(name);
		this.setPassword(password);
		this.setEmail(email);
		groups = new ResponseListModel<Group>();
	}
	
	/**
	 * Join group (insert group in the list)
	 * @param group = group object
	 */
	public void joinGroup(Group group) {
		groups.add(group);
	}
	
	/**
	 * Leave group (remove group from the list)
	 * @param group = group object
	 */
	public void leaveGroup(Group group) {
		groups.remove(group);
	}
	
	/**
	 * Get the list of joined groups
	 * @return list of joined groups (List<Group>)
	 */
	public ResponseListModel<Group> getGroups() {
		return groups;
	}
	
	/**
	 * Set user name
	 * @param name = user name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get user name
	 * @return user name (String)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set user password
	 * @param password = user password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Get user password
	 * @return user password (String)
	 */
	public String getPassword() {
		return password;
	}
		
	
	/**
	 * Set user email
	 * @param email = user email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Get user email
	 * @return user email (String)
	 */
	public String getEmail() {
		return email;
	}
	
	@Override
	public String toString() {
		return this.getName();
	}
	
	@Override 
	public boolean equals(Object o) {
		return (this.getName().compareTo(
				((User)o).getName()) == 0);
	}
}
