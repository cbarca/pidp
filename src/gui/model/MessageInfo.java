package gui.model;

import java.awt.Color;

public class MessageInfo {
	private User user;
	private String message;
	private Color color;
	private String font;
	
	public MessageInfo(User user, String message, Color color, String font) {
		this.user = user;
		this.message = message;
		this.color = color;
		this.font = font;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}
	
	

}
