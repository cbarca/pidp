package gui.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.HashMap;
import java.util.Map;
import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;

import gui.drawing.*;

/**
 * 
 * @author Cristi
 *
 */
public class Group implements Serializable {
	/**
	 * Constants
	 */
	public static final String DEFAULT_NAME = "None";
	public static final int MAX_USERS = 50;
	
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private ResponseListModel<User> users;
	private Map<User, Color> mapColor;
	private List<Color> availColors;
	private String font = Font.SERIF;
	private Color textColor = Color.red;
	private List<Drawing> drawings;
	private List<MessageInfo> loggedMessages;
	private String selectedShape = null;
	
	
	
	/**
	 * Default constructor
	 */
	public Group() {
		this(DEFAULT_NAME);
	}
	
	/**
	 * Specialized constructor
	 * @param name = group name
	 */
	public Group(String name) {
		this.setName(name);
		users = new ResponseListModel<User>();
		mapColor = new HashMap<User, Color>();
		availColors = new ArrayList<Color>();
		drawings = new ArrayList<Drawing>();
		loggedMessages = new ArrayList<MessageInfo>();
		
		
		for (int i = 0; i < MAX_USERS; i++) {
		    availColors.add(Color.getHSBColor((float) i / MAX_USERS, 1, 1));
		}
	}
	
	/**
	 * Get the list of available colors
	 * @return list of available colors (List<Color>)
	 */
	public List<Color> getAvailColors() {
		return availColors;
	}
	
	/**
	 * Set user color (unique color for each user)
	 * @param user = user object
	 * @param color = select color (from a visual list)
	 */
	private void setUserColor(User user, Color color) {
		mapColor.put(user, color);
		
		Iterator<Color> it = availColors.iterator();
		while (it.hasNext()) {
			Color col = it.next();
			
			if (col.equals(color)) {
				it.remove();
				break;
			}
		}
	}
	
	/**
	 * Get user color
	 * @param user = user object
	 * @return the color associate with user (Color)
	 */
	public Color getUserColor(User user) {
		return mapColor.get(user);
	}
	
	/**
	 * Set group name
	 * @param name = group name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get group name
	 * @return group name (String)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Add user into this group
	 * @param user = user object
	 */
	private void addUser(User user) {
		users.add(user);
	}
	
	/**
	 * Add user into this group and associate color
	 * @param user = user object
	 * @param color = color object
	 */
	public void addUser(User user, Color color) {
		this.addUser(user);
		this.setUserColor(user, color);
	}
	
	/**
	 * Remove user from this group and associate color
	 * @param user = user object
	 */
	public void removeUser(User user) {
		users.remove(user);
		availColors.add(mapColor.get(user));
		mapColor.remove(user);
	}
	
	/**
	 * Get users from this group
	 * @return the list of users from the group
	 */
	public ResponseListModel<User> getUsers() {
		return users;
	}
	
	/**
	 * Add a new drawing to the list
	 * @param shape
	 */
	public void logShape(Drawing shape) {
		drawings.add(shape);
	}
	
	/**
	 * Log a new message in the group's list
	 * @param msgInfo
	 */
	public void logMessage(MessageInfo msgInfo) {
		loggedMessages.add(msgInfo);
	}
	
	
	
	@Override	
	public String toString() {
		return this.getName();
	}

	/**
	 * Set font of the message in the group's messages list
	 * @param font
	 */
	public void setFont(String font) {
		this.font = font;
	}

	/**
	 * Get the group's font
	 * @return
	 */
	public String getFont() {
		return font;
	}

	/**
	 * Set color of the text in the group's messages list
	 * @param textColor
	 */
	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	/**
	 * Get color set for the groups
	 * @return
	 */
	public Color getTextColor() {
		return textColor;
	}

	/**
	 * Get list of all drawings of the group
	 * @return
	 */
	public List<Drawing> getDrawings() {
		return drawings;
	}

	/**
	 * Get list of the logged messages in the group
	 * @return
	 */
	public List<MessageInfo> getLoggedMessages() {
		return loggedMessages;
	}

	/**
	 * Set the current shape selected for drawing
	 * @param selectedShape
	 */
	public void setSelectedShape(String selectedShape) {
		this.selectedShape = selectedShape;
	}

	/**
	 * Return the shape currently selected for the group
	 * @return
	 */
	public String getSelectedShape() {
		return selectedShape;
	}
}
