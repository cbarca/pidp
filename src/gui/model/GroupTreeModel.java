package gui.model;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

public class GroupTreeModel extends DefaultTreeModel implements ListDataListener {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;
	
	public GroupTreeModel(TreeNode root) {
		super(root);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void contentsChanged(ListDataEvent arg0) {
		// TODO Auto-generated method stub
		this.intervalAdded(arg0);	
	}

	@SuppressWarnings("unchecked")
	@Override
	public void intervalAdded(ListDataEvent arg0) {
		// TODO Auto-generated method stub
		ResponseListModel<Group> list = (ResponseListModel<Group>)arg0.getSource();
		((DefaultMutableTreeNode)this.getRoot()).removeAllChildren();
		
		for (Group group : list) {		
			DefaultMutableTreeNode mgroup = new DefaultMutableTreeNode(group);
			for (User user : group.getUsers()) {
				DefaultMutableTreeNode muser = new DefaultMutableTreeNode(user);
				mgroup.add(muser);
			}
		
			((DefaultMutableTreeNode)this.getRoot()).add(mgroup);
		}
		
		this.reload((TreeNode)this.getRoot());
	}

	@Override
	public void intervalRemoved(ListDataEvent arg0) {
		// TODO Auto-generated method stub
		this.intervalAdded(arg0);
	}

		
}
