package gui.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import gui.model.*;
import gui.view.*;
import mediator.Mediator;

public class GroupTreeController {
	/**
	 * PrivateMembers
	 */
	private GroupTreeView grpTreeView;
	private WhiteboardController whiteboardController;
	private Mediator mediator;
	private GrpTreeMenuView treeMenu;
	private GrpTreeMenuController treeMenuCtrl;

	/**
	 * Specialized constructor
	 * @param mediator = mediator
	 * @param grpTreeView = group tree view
	 */
	public GroupTreeController(Mediator mediator, GroupTreeView grpTreeView) {
		this.mediator = mediator;
		this.grpTreeView = grpTreeView;
		this.grpTreeView.addTreeMouseAdapter(new TreeMouseAdapter());
	}
	
	public void setWhiteboardController(WhiteboardController whiteboardController) {
		this.whiteboardController = whiteboardController;
	}
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class TreeMouseAdapter extends MouseAdapter {
		int x, y;
		JTree tree;
		TreePath path;
		
		private void myPopupEvent(MouseEvent e) {
			x = e.getX();
			y = e.getY();
			tree = (JTree)e.getSource();
			path = tree.getPathForLocation(x, y);
			
			if (path == null)
				return; 

			tree.setSelectionPath(path);

			DefaultMutableTreeNode obj = (DefaultMutableTreeNode)path.getLastPathComponent();
			
			if (obj.getUserObject() instanceof Group) {
				treeMenu = new GrpTreeMenuView((Group)obj.getUserObject());
				treeMenuCtrl = new GrpTreeMenuController(mediator, treeMenu, whiteboardController);
				treeMenu.show(tree, x, y);
			}
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) myPopupEvent(e);
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) myPopupEvent(e);
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			checkPopup(e);
		}
		
		private void checkPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				treeMenu.show(tree, x, y);
				whiteboardController.redraw(mediator.getCurrentUser().getGroups());
			}
		}
	}
}
