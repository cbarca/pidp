package gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JOptionPane;

import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;

import gui.util.DialogSelector;
import gui.util.ExportTask;
import gui.view.*;
import mediator.Mediator;

public class CommandBarController {
	/**
	 * Private members
	 */
	private CommandBarView cmdBarView;
	private WhiteboardController whiteboardController;
	private Mediator mediator;

	/**
	 * Specialized constructor.
	 * @param mediator = mediator object
	 * @param cmdBarView = command bar view object
	 */
	public CommandBarController(Mediator mediator, CommandBarView cmdBarView) {
		this.cmdBarView = cmdBarView;
		this.mediator = mediator;

		this.addListeners();
	}

	/**
	 * Add listeners to command bar view
	 */
	private void addListeners() {
		cmdBarView.addLogoutListener(new LogoutListener());
		cmdBarView.addCreateGroupListener(new CreateGroupListener());
		cmdBarView.addSaveGoupWorkListener(new SaveGoupWorkListener());
	}

	/**
	 * Add controller for whiteboard
	 * @param whiteboardController
	 */
	public void setWhiteboardController(WhiteboardController whiteboardController) {
		this.whiteboardController = whiteboardController;
	}

	class SaveGoupWorkListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			ExportTask task = new ExportTask();
			task.addPropertyChangeListener(new ProgressBarListener());
			task.execute();
			
			cmdBarView.disableSaveBtn(true);
		}
	}
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class LogoutListener implements ActionListener {
		/**
		 * logout action: reeboot to login page
		 */
		public void actionPerformed(ActionEvent e) {
			String[] args = new String[1];
        	args[0] = mediator.getCurrentUser().getName();
        	mediator.getClient().send(
        			GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
        											ECmdMessage.ReqLogoutUser, 
        											args).getBytes());
        	
			mediator.logoutUser();
			mediator.getBootstrapper().BootLogin();
		}
	}

	/**
	 * 
	 * @author Cristi
	 *
	 */
	class CreateGroupListener implements ActionListener {
		/**
		 * create group action: add a new group with current user in it
		 */
		public void actionPerformed(ActionEvent e) {
			// Dialog color-selector
			DialogSelector dialSel = new DialogSelector();
			dialSel.setLocationRelativeTo(cmdBarView);
			GroupSelectorView grpSelView = new GroupSelectorView(mediator.getGroupList());
			GroupSelectorController grpSelCtrl = new GroupSelectorController(mediator, grpSelView, whiteboardController);
			dialSel.setContentPane(grpSelView);
			dialSel.setVisible(true);
			dialSel.pack();
			dialSel.setAlwaysOnTop(true);
			dialSel.requestFocus();
		}
	}

	/**
	 * 
	 * @author Cristi
	 *
	 */
	class ProgressBarListener implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			// TODO Auto-generated method stub
			if(evt.getPropertyName().equals("progress")) {
				cmdBarView.setSavingProgress((Integer)evt.getNewValue());
				if ((Integer)evt.getNewValue() == 0) {
					Mediator.getInstance().saveGroupWork();
					JOptionPane.showMessageDialog(null, "Done with saving...");
					cmdBarView.disableSaveBtn(false);
				}
			}
		}
	}

}
