package gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;

import mediator.Mediator;
import gui.view.*;

public class GroupSelectorController {
	private GroupSelectorView grpSelView;
	private WhiteboardController whiteboardController;
	private Mediator mediator;
	private String groupName = null;
	
	/**
	 * Specialized constructor
	 * @param mediator = mediator 
	 * @param grpSelView  = group selector view 
	 * @param whiteboardController = whiteboard controller 
	 */
	public GroupSelectorController(Mediator mediator, GroupSelectorView grpSelView, WhiteboardController whiteboardController) {
		this.mediator = mediator;
		this.grpSelView = grpSelView;
		this.whiteboardController = whiteboardController;
		
		this.AddListeners();
	}
	
	private void AddListeners() {
		this.grpSelView.addBtnOkListener(new BtnOkListener());
		this.grpSelView.addGroupNameListener(new GroupNameListener());
	}
	
	class BtnOkListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			boolean isIn = false;
			
			isIn = mediator.groupExists(groupName);
			
			if (isIn || groupName == " " || groupName == null) {
				JOptionPane.showMessageDialog(null, "You are doing it wrong: insert another name! :)");	
			}
			else {
				mediator.addNewGroup(mediator.getCurrentUser().getName(), groupName);
				whiteboardController.redraw(mediator.getCurrentUser().getGroups());
				((JButton)arg0.getSource()).setEnabled(false);	
				
				//send message to server
				String[] args = new String[2];
	        	args[0] = mediator.getCurrentUser().getName();
	        	args[1] = groupName;
	        	mediator.getClient().send(
	        			GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
	        											ECmdMessage.AddNewGroup,
	        											args).getBytes());
			}
		}	
	}
	
	class GroupNameListener implements DocumentListener {

		@Override
		public void changedUpdate(DocumentEvent arg0) {
			// TODO Auto-generated method stub
			groupName = grpSelView.getGroupName();
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			// TODO Auto-generated method stub
			groupName = grpSelView.getGroupName();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			// TODO Auto-generated method stub
			groupName = grpSelView.getGroupName();
		}
	}
}
