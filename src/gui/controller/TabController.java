package gui.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.JComboBox;

import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;

import gui.model.User;
import gui.view.*;
import mediator.*;
import gui.model.*;
import gui.drawing.*;

/**
 * 
 * @author ana
 *
 */
public class TabController {
	
	/**
	 * Private members
	 */
	private Mediator mediator;
	private TabView tabView;
	private Group group;
	
	/**
	 * Specialized constructor
	 * @param mediator = mediator
	 * @param tabView = tab view
	 * @param group = group (associated group)
	 */
	public TabController(Mediator mediator, TabView tabView, Group group) {
		this.mediator = mediator;
		this.tabView = tabView;
		this.group = group;
		
		addListeners();
	}
	
	/**
	 * Add event listeners for the components
	 */
	private void addListeners() {
		tabView.addColorListener(new ColorListener());
		tabView.addFontListener(new FontListener());
		tabView.addMessageListener(new MessageListener());
		tabView.addDrawingButtonListener(new DrawingButtonListener());
		tabView.addCanvasListsener(new CanvasListener());
		tabView.addRedrawListener(new UpdateListener());
	}
	
	/**
	 * 
	 * @author ana
	 * Text color listener
	 */
	class ColorListener implements ActionListener {
		
        public void actionPerformed(ActionEvent e) {
        	JComboBox cb = (JComboBox)e.getSource();
        	String color = (String)cb.getSelectedItem();
        	
        	//default color
        	Color tabColor = Color.red;
        	
        	if (color.equals("Red"))
        		tabColor = Color.red;
        	if (color.equals("Green"))
        		tabColor = Color.green;
        	if (color.equals("Blue"))
        		tabColor = Color.blue;
        	if (color.equals("Black"))
        		tabColor = Color.black;
        	
        	group.setTextColor(tabColor);
        }
    }
	
	/**
	 * 
	 * @author ana
	 * Text font listener
	 */
	class FontListener implements ActionListener {
		
        public void actionPerformed(ActionEvent e) {
        	JComboBox cb = (JComboBox)e.getSource();
        	String font = (String)cb.getSelectedItem();
        	
        	group.setFont(font);
        }
    }
	
	/**
	 * 
	 * @author ana
	 * Message listener
	 */
	class MessageListener implements ActionListener {
		
        public void actionPerformed(ActionEvent e) {
        	String message = tabView.getMessageText();
        	User currentUser = mediator.getCurrentUser();
        	MessageInfo mi = new MessageInfo(currentUser, message, group.getTextColor(), group.getFont());
        	
        	String[] args = new String[3];
        	args[0] = mediator.getCurrentUser().getName();
        	args[1] = group.getName();
        	args[2] = message;
        	mediator.getClient().send(
        			GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
        											ECmdMessage.SendMessageToGroup, 
        											args).getBytes());
        	
        	group.logMessage(mi);
        	tabView.clearText();
        	tabView.updateDisplayedMessages(group.getLoggedMessages());
        	
        }
    }
	
	/**
	 * 
	 * @author ana
	 * Shape selection listener
	 */
	class DrawingButtonListener implements ActionListener {
		
        public void actionPerformed(ActionEvent e) {
        	AbstractButton ab = (AbstractButton)e.getSource();
        	boolean selected = ab.isSelected();
        	String buttonName = ab.getName();

        	if (selected) {
        		group.setSelectedShape(buttonName);
        		tabView.deselectButtons(buttonName);
        	}
        	else
        		group.setSelectedShape(null);
        }
    }
	
	/**
	 * 
	 * @author ana
	 * Canvas listener
	 */
	class CanvasListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			int x = arg0.getX(), y = arg0.getY();
			
			if (group.getSelectedShape() != null) {
				Drawing shape = null;
				Color c = group.getUserColor(mediator.getCurrentUser());
				
				if (group.getSelectedShape().equals("Square")) {
					shape = new Square(x, y, c);
				}
				
				if (group.getSelectedShape().equals("Circle")) {
					shape = new Circle(x, y, c);
				}
				
				if (group.getSelectedShape().equals("Line")) {
					shape = new Line(x, y, c);
				}
				
				if (group.getSelectedShape().equals("Arrow")) {
					shape = new Arrow(x, y, c);
				}
				group.logShape(shape);
				tabView.draw(group.getDrawings());	
				
				String[] args = new String[5];
	        	args[0] = mediator.getCurrentUser().getName();
	        	args[1] = group.getName();
	        	args[2] = group.getSelectedShape();
	        	args[3] = x + "";
	        	args[4] = y + "";
	        	
	        	mediator.getClient().send(
	        			GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
	        											ECmdMessage.DrawShapeToGroup, 
	        											args).getBytes());
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			tabView.draw(group.getDrawings());
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

	}
	
	class UpdateListener implements KeyListener {

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			tabView.draw(group.getDrawings());
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}

}
