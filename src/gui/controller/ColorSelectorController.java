package gui.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;

import gui.view.*;
import mediator.Mediator;

public class ColorSelectorController {
	/**
	 * Private members
	 */
	private Mediator mediator;
	private ColorSelectorView colSelView;
	private Color userColor;
	private WhiteboardController whiteboardController;
	
	/**
	 * Specialized constructor
	 * @param mediator = mediator 
	 * @param colSelView = color selector view
	 * @param whiteboardController = whiteboard controller  
	 */
	public ColorSelectorController(Mediator mediator, ColorSelectorView colSelView, WhiteboardController whiteboardController) {
		this.mediator = mediator;
		this.colSelView = colSelView;
		this.whiteboardController = whiteboardController;
		userColor = colSelView.getSelectedColor();
		
		this.AddListeners();
	}
	
	private void AddListeners() {
		colSelView.addSelectBtnListener(new SelectBtnListener());
		colSelView.addSelectBoxListener(new SelectBoxListener());
	}
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class SelectBtnListener implements ActionListener {
		/**
		 * reset action: reset username & password
		 */
        public void actionPerformed(ActionEvent e) {        	
        	mediator.addUserToGroup(colSelView.getUser().getName(), colSelView.getGroup().getName(), 
        			userColor.getRed(), userColor.getGreen(), userColor.getBlue());
			whiteboardController.redraw(mediator.getCurrentUser().getGroups());
			((JButton)e.getSource()).setEnabled(false);
			
			String[] args = new String[5];
        	args[0] = colSelView.getUser().getName();
        	args[1] = colSelView.getGroup().getName();
        	args[2] = userColor.getRed() + "";
        	args[3] = userColor.getGreen() + "";
        	args[4] = userColor.getBlue() + "";
        	mediator.getClient().send(
        			GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
        											ECmdMessage.AddUserToGroup,
        											args).getBytes());
        }
    }
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class SelectBoxListener implements ActionListener {
		/**
		 * reset action: reset username & password
		 */
        public void actionPerformed(ActionEvent e) {
        	userColor = colSelView.getSelectedColor();
        }
    }
}
