package gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;

import mediator.Mediator;
import gui.view.*;

/**
 * 
 * @author Cristi
 *
 */
public class LoginController {
	/**
	 * Private members
	 */
	private LoginView loginView;
	private Mediator mediator;
	
	/**
	 * Specialized constructor.
	 * @param mediator = mediator 
	 * @param loginView = login view 
	 */
	public LoginController(Mediator mediator, LoginView loginView) {
		this.loginView = loginView;
		this.mediator = mediator;
		
		this.addListeners();
	}
	
	/**
	 * Add listeners to login view 
	 */
	private void addListeners() {
		loginView.addLoginListener(new LoginListener());
		loginView.addResetListener(new ResetListener());
	}
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class LoginListener implements ActionListener {
		/**
		 * login action: verify username & password
		 * 		if [ok] => change the content pane
		 */
        public void actionPerformed(ActionEvent e) {
        	String username = loginView.getUsername(),
        		password = loginView.getPassword(),
        		email = loginView.getEmail();
        	
        	// mediator.loginUser(username);
        	
        	//send message to server
			String[] args = new String[3];
        	args[0] = username;
        	args[1] = password;
        	args[2] = email;
        	mediator.getClient().send(
        			GenerateCmdMessage.generateCSVCmd(username, 
        											ECmdMessage.ReqLoginUser,
        											args).getBytes());
        }
    }
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class ResetListener implements ActionListener {
		/**
		 * reset action: reset username & password
		 */
        public void actionPerformed(ActionEvent e) {
        	loginView.resetUsername();
        	loginView.resetPassword();
        }
    }
}
