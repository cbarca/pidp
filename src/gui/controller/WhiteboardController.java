package gui.controller;

import gui.model.Group;
import gui.model.ResponseListModel;
import gui.view.TabView;
import gui.view.WhiteboardView;

import javax.swing.JTabbedPane;

import mediator.Mediator;

public class WhiteboardController {
	/**
	 * Private members
	 */
	private ResponseListModel<Group> groups;
	private WhiteboardView whiteboarView;
	private Mediator mediator;
	
	/**
	 * Specialized constructor 
	 * @param groups = list of current groups
	 * @param whiteboardView = whiteboard view
	 * @param mediator = mediator
	 */
	public WhiteboardController(ResponseListModel<Group> groups, WhiteboardView whiteboardView, Mediator mediator) {
		this.groups = groups;
		this.whiteboarView = whiteboardView;
		this.mediator = mediator;
	}
	
	/**
	 * Modify tabs in the view when a change occurs
	 * @param groups
	 */
	public void redraw(ResponseListModel<Group> groups) {
		JTabbedPane tabbedPane = whiteboarView.getTabbedPane();
		
		//scan for new tabs
		for (int i=0; i<groups.size(); i++) {
			boolean alreadyIn = false;
			int index = 0;
			
			for (int j=0; j<tabbedPane.getTabCount(); j++) {
				String title = tabbedPane.getTitleAt(j);
				if (title.equals(groups.get(i).getName())) {
					alreadyIn = true;
					index = j;
				}
			}
			
			if (!alreadyIn) {
				TabView tabView = new TabView(groups.get(i));
				tabbedPane.addTab(groups.get(i).getName(), tabView);
				TabController tabController = new TabController(mediator, tabView, groups.get(i));
				tabView.restoreHistory(groups.get(i));
			}
			else {
				TabView tv = (TabView)tabbedPane.getComponent(index);
				tv.restoreHistory(groups.get(i));
			}
		}
		
		//remove old tabs
		for (int j=0; j<tabbedPane.getTabCount(); j++) {
			String title = tabbedPane.getTitleAt(j);
			boolean stillIn = false;
			
			for (int i=0; i<groups.size(); i++)
				if (title.equals(groups.get(i).getName()))
					stillIn = true;
			
			if (!stillIn)
				tabbedPane.remove(j);
		}
	}
	
	/**
	 * Update drawings in existing tabs
	 * @param groups
	 */
	public void updateDrawings(Group group) {
		JTabbedPane tabbedPane = whiteboarView.getTabbedPane();
		
		for (int i=0; i<tabbedPane.getTabCount(); i++) {
			TabView tv = (TabView)tabbedPane.getComponent(i);
			if (tv.getGroupName().equals(group.getName()))
				tv.draw(group.getDrawings());
		}
	}
	
	/**
	 * Update messages
	 * @param groups
	 */
	public void updateTextMessages(Group group) {
		JTabbedPane tabbedPane = whiteboarView.getTabbedPane();
		
		for (int i=0; i<tabbedPane.getTabCount(); i++) {
			TabView tv = (TabView)tabbedPane.getComponent(i);
			if (tv.getGroupName().equals(group.getName()))
				tv.updateDisplayedMessages(group.getLoggedMessages());
		}
	}

}
