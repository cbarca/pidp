package gui.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import net.model.ECmdMessage;
import net.util.GenerateCmdMessage;

import gui.util.DialogSelector;
import gui.model.*;
import gui.view.*;
import mediator.Mediator;

public class GrpTreeMenuController {
	/**
	 * Private members
	 */
	private Mediator mediator;
	private GrpTreeMenuView treeMenu;
	private WhiteboardController whiteboardController;
	
	/**
	 * Specialized constructor
	 * @param mediator = mediator
	 * @param treeMenu = tree menu (popup)
	 * @param whiteboardController = whiteboard controller
	 */
	public GrpTreeMenuController(Mediator mediator, GrpTreeMenuView treeMenu, WhiteboardController whiteboardController) {
		this.mediator = mediator;
		this.treeMenu = treeMenu;
		this.whiteboardController = whiteboardController;
		
		this.refreshMenu();
		this.addListeners();
	}
	
	private void addListeners() {
		treeMenu.addAddUsrListener(new AddUsrListener());
		treeMenu.addJoinGrpListener(new JoinUsrListener());
		treeMenu.addLeaveGrpListener(new LeaveUsrListener());
	}
	
	/**
	 * Refresh the menu (setting visible/invisble Join/Leave buttons)
	 */
	private void refreshMenu() {
		treeMenu.setVisibleJoin();
		treeMenu.setInvisibleLeave();
		
		for (User user : treeMenu.getOnGroup().getUsers()) {
			if (mediator.getCurrentUser().equals(user)) {
				treeMenu.setInvisibleJoin();
				treeMenu.setVisibleLeave();
				break;
			}
		}
	}
	
	class AddUsrListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (mediator.getSelectedUser() == null) {
				JOptionPane.showMessageDialog(null, "Y u don't select a user? :)");
			}
			else {
				boolean isIn = false;
				
				for (User user : treeMenu.getOnGroup().getUsers()) {
					if (mediator.getSelectedUser().equals(user)) {
						isIn = true;
						break;
					}
				}
			
				if (isIn) {
					JOptionPane.showMessageDialog(null, "You are doing it wrong: select another user! :)");
				}
				else {
					// Dialog color-selector
					DialogSelector dialSel = new DialogSelector();
					dialSel.setLocationRelativeTo(treeMenu);
					ColorSelectorView colSelView = new ColorSelectorView(treeMenu.getOnGroup(), mediator.getSelectedUser());
					ColorSelectorController colSelCtrl = new ColorSelectorController(mediator, colSelView, whiteboardController);
					dialSel.setContentPane(colSelView);
					dialSel.setVisible(true);
					dialSel.pack();
					dialSel.setAlwaysOnTop(true);
					dialSel.requestFocus();
					
				}
			}
		}
	}
	
	class JoinUsrListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			// Window color-selector
			DialogSelector dialSel = new DialogSelector();
			dialSel.setLocationRelativeTo(treeMenu);
			ColorSelectorView colSelView = new ColorSelectorView(treeMenu.getOnGroup(), mediator.getCurrentUser());
			ColorSelectorController colSelCtrl = new ColorSelectorController(mediator, colSelView, whiteboardController);
			dialSel.setContentPane(colSelView);
			dialSel.setVisible(true);
			dialSel.pack();
			dialSel.setAlwaysOnTop(true);
			dialSel.requestFocus();
			
		}
	}
	
	class LeaveUsrListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			mediator.userLeaveGroup(mediator.getCurrentUser().getName(), treeMenu.getOnGroup().getName());
			
			String[] args = new String[2];
        	args[0] = mediator.getCurrentUser().getName();
        	args[1] = treeMenu.getOnGroup().getName();
        	mediator.getClient().send(
        			GenerateCmdMessage.generateCSVCmd(mediator.getCurrentUser().getName(), 
        											ECmdMessage.UserLeaveGroup,
        											args).getBytes());
			
			whiteboardController.redraw(mediator.getCurrentUser().getGroups());
			treeMenu.hideBox();
		}
	}
}
