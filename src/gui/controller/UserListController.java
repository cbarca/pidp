package gui.controller;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import mediator.Mediator;
import gui.model.*;
import gui.view.*;

public class UserListController {
	private ResponseListModel<User> users;
	private UserListView ulView;
	private Mediator mediator;
	
	/**
	 * Specialized constructor
	 * @param mediator = mediator
	 * @param ulView = user list view
	 */
	public UserListController(Mediator mediator, UserListView ulView) {
		this.users = mediator.getUserList();
		this.ulView = ulView;
		this.mediator = mediator;
		
		this.addListeners();
	}
	
	private void addListeners() {
		ulView.addUserListListener(new UserListListener());
	}
	
	/**
	 * 
	 * @author Cristi
	 *
	 */
	class UserListListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent arg0) {
	        if (!arg0.getValueIsAdjusting()) {
	        	mediator.setSelectedUser(ulView.getSelectedUser());
	        }
		}  
    }
}
