package gui.util;

import javax.swing.JDialog;
import javax.swing.JPanel;

public class DialogSelector extends JDialog {
	/**
	 * Private members
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public DialogSelector() {
		super();
		setBounds(100, 100, 300, 200);
	}
	
	public void insertPanel(JPanel panel) {
		this.setContentPane(panel);
	}

}
