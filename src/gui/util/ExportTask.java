package gui.util;

import java.util.List;

import javax.swing.SwingWorker;

public class ExportTask extends SwingWorker<Integer, Integer> {
	public ExportTask() {
	}

	@Override
	protected Integer doInBackground() throws Exception {
		int DELAY = 100;
		int count = 100;
		int i     = 1;
		try {
			while (i < count) {
				i++;
				Thread.sleep(DELAY);
				publish(i);
				setProgress(i);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	protected void process(List<Integer> chunks) {
		
	}

	@Override
	protected void done() {
		if (isCancelled())
			System.out.println("Cancelled !");
		else {
			System.out.println("Done !");
			setProgress(0);
		}	
	}
}
