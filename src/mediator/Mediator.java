package mediator;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.google.gdata.client.authn.oauth.OAuthException;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import net.Client;
import net.controller.ClientWorker;

import gdl.DocumentList;
import gdl.DocumentListException;
import gui.util.*;
import gui.Bootstrapper;
import gui.Shell;
import gui.controller.WhiteboardController;
import gui.drawing.*;
import gui.model.*;

/**
 * Mediator Class - singleton pattern
 * Implements the functionalities behind the GUI
 * @author Cristi
 *
 */
public class Mediator implements Serializable {
	/**
	 * Private members 
	 */
	private static final long serialVersionUID = 1L;
	private ResponseListModel<User> users = null;
	private List<Pair<String, String>> authUsers = null;
	private ResponseListModel<Group> groups = null;
	private User currUser = null, selUser = null;
	private Bootstrapper boot = null;
	private WhiteboardController whiteboardController = null;
	private Lock mlock;
	private Client client;
	private DocumentList docList;
	private String username;
	
	// Singleton object
	private static Mediator mediator = null;
	
	/**
	 * Constructor: Server_use
	 */
	public Mediator() {
		this(null, null);
	}
	
	/**
	 * Constructor: ClientGUI_use 
	 * @param boot = bootstrapper
	 * @param client = client
	 */
	@SuppressWarnings("static-access")
	private Mediator(Bootstrapper boot, String username) {
		this.setUsername(username);
		if (boot != null) {
			this.boot = boot;
			try {
				ClientWorker worker = new ClientWorker();
				client = new Client(InetAddress.getLocalHost(), 10000, this, worker);
				worker.setLogger(client.getLogger());
				Thread w = new Thread(worker);
				w.setDaemon(true);
				w.start();
				Thread t = new Thread(client);
				t.setDaemon(true);
				t.start();
				//wait to have the connection set
				t.sleep(10);
				
				docList = DocumentList.getInstance();
				
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else {
			authUsers = new ArrayList<Pair<String, String>>();	
			this.syncUsers("users.txt");
		}
						
		users = new ResponseListModel<User>();	
		groups = new ResponseListModel<Group>();
		mlock = new ReentrantLock();
	}
	
	/**
	 * Get instance
	 * @return mediator instance (for Server use)
	 */
	public static Mediator getInstance() {
		return Mediator.getInstance(null, null);
	}
	
	/**
	 * Get instance (within bootstrapper)
	 * @param boot = bootstrapper
	 * @param client = client
	 * @return  mediator instance (for ClientGUI use)
	 */
	public static Mediator getInstance(Bootstrapper boot, String username) {
		if (mediator == null) {
			mediator = new Mediator(boot, username);
		}
				
		return mediator;
	}
	
	/**
	 * Get bootstrapper
	 * @return bootstrapper instance
	 */
	public Bootstrapper getBootstrapper() {
		return boot;
	}
	
	/**
	 * Get client
	 * @return client instance
	 */
	public Client getClient() {
		return client;
	}
	
	/*
	 * GUI functions
	 */
	
	public void syncUsers(String filename) {
		BufferedReader input;
		try {
			input = new BufferedReader(new FileReader(filename));
			String line, username, passwd;
						
			authUsers.clear();
			while ((line = input.readLine()) != null) {
				
				// line: username password
				
				username = line.split(" ")[0];
				passwd = line.split(" ")[1];
				
				authUsers.add(new Pair<String, String>(username, passwd));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void syncState(Mediator globalMed) {
		users = globalMed.users;
		groups = globalMed.groups;
	}
	
	public ResponseListModel<User> getUserList() {
		return users;
	}
	
	public ResponseListModel<Group> getGroupList() {
		return groups;
	}
	
	public User getCurrentUser() {
		return currUser;
	}
	
	public void setCurrentUser(User currUser) {
		this.currUser = currUser;
	}
		
	public void setSelectedUser(User selUser) {
		this.selUser = selUser;
	}
	
	public User getSelectedUser() {
		return selUser;
	}
		
	public User getUserByName(String name) {
		name = name.replaceAll("\\r|\\n", "");
			
		for (User user : this.getUserList()) {
			if (user.getName().compareTo(name) == 0) {
				return user;
			}
		}
		
		return null;
	}
	
	public Group getGroupByName(String name) {	
		name = name.replaceAll("\\r|\\n", "");
				
		for (Group group : this.getGroupList()) {
			if (group.getName().compareTo(name) == 0) {
				return group;
			}
		}
		
		return null;
	}
	
	/**
	 * Include another user in chat
	 * @param username
	 */
	public User includeUser(String username, String password, String email) {
		// lock
		mlock.lock();
		
		User u = new User(username, password, email);
		Group g = this.getGroupByName(username);
		
		if (g == null) {
			g = new Group(username);
			g.addUser(u, g.getAvailColors().get(0));
			groups.add(g);
		}
		else {
			g.addUser(u, g.getAvailColors().get(0));
			
			// workaround - to trigger the listener
			Group bullshit = new Group("o_O");
			groups.add(bullshit);
			groups.remove(bullshit);
		}
				
		u.joinGroup(g);
		users.add(u);
		
		// unlock
		mlock.unlock();
		
		return u;
	}
	
	/**
	 * Exclude an user from chat
	 * @param username
	 */
	public void excludeUser(String username) {
		// lock
		mlock.lock();
		
		ResponseListModel<Group> ugroups = new ResponseListModel<Group>();
		User user = this.getUserByName(username);
				
		for (Group group : user.getGroups()) {
			group.removeUser(user);
			ugroups.add(group);
		}
		for (Group group : ugroups) {
			user.leaveGroup(group);
		}
		ugroups.clear();

		Iterator<Group> it = groups.iterator();
		while (it.hasNext()) {
			Group group = (Group)it.next();

			if (group.getUsers().size() == 0) {
				it.remove();
			}
		}

		users.remove(user);
		
		// workaround - to trigger the listener
		Group bullshit = new Group("o_O");
		groups.add(bullshit);
		groups.remove(bullshit);
		
		if (whiteboardController != null) {
			whiteboardController.redraw(currUser.getGroups());
		}
		
		// unlock
		mlock.unlock();
	}
	
	/**
	 * Login user - for current user
	 * @param username
	 * @param password
	 */
	public void loginUser(String username) {
		// lock
		mlock.lock();
		
		this.setCurrentUser(this.getUserByName(username));
		this.getBootstrapper().BootTo(new Shell(mediator));
		
		try {
			
			docList.getAccessToken();
			docList.loginService();
			
		} catch (OAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// unlock
		mlock.unlock();
	}
	
	/**
	 * Verify user and then permit login by calling loginUser method
	 * @param username
	 * @param password
	 * @return success/not
	 */
	public boolean verifUser(String username, String password) {
		// lock
		mlock.lock();
			
		if (this.getUserByName(username) != null) {
			return false;
		}
		
		for (int i = 0; i < authUsers.size(); i++) {
			Pair<String, String> up = authUsers.get(i);
						
    		if (username.compareTo(up.fst) == 0 &&
    				password.compareTo(up.snd) == 0) {    			
    			return true;
    		}
    	}
				
		// unlock
		mlock.unlock();
		
		return false;
	}
	
	/**
	 * Logout current user
	 */
	public void logoutUser() {
		// lock
		mlock.lock();
		
		this.excludeUser(this.getCurrentUser().getName());		
		this.setCurrentUser(null);
		
		try {
			docList.logoutService();
		} catch (OAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// empty lists !!!
		
		// unlock
		mlock.unlock();
	}
	
	/**
	 * Leave a group
	 * @param username
	 * @param groupname
	 */
	public void userLeaveGroup(String username, String groupname) {
		// lock
		mlock.lock();
		
		User user = this.getUserByName(username);
		Group group = this.getGroupByName(groupname);
		
		group.removeUser(user);
		
		if (group.getUsers().size() == 0) {
			groups.remove(group);
		}
		else {
			// workaround - to trigger the listener
			Group bullshit = new Group("o_O");
			groups.add(bullshit);
			groups.remove(bullshit);
		}
		
		user.leaveGroup(group);
		
		if (whiteboardController != null) {
			whiteboardController.redraw(currUser.getGroups());
		}
		
		// unlock
		mlock.unlock();
	}
	
	/**
	 * Tests if the group already exists
	 * @param groupname
	 * @return
	 */
	public boolean groupExists(String groupname) {
		// lock
		mlock.lock();
		
		boolean isIn = false;
		
		for (Group grp : groups) {
			if (grp.getName().compareTo(groupname) == 0) {
				isIn = true;
				break;
			}
		}
		
		// unlock
		mlock.unlock();
		
		return isIn;
	}
	
	/**
	 * Add new group
	 * @param username
	 * @param groupname
	 */
	public void addNewGroup(String username, String groupname) {
		// lock
		mlock.lock();
		
		User user = this.getUserByName(username);
		Group group = new Group(groupname);
		group.addUser(user, group.getAvailColors().get(0));
		groups.add(group);
		user.joinGroup(group);
		
		if (whiteboardController != null) {
			whiteboardController.redraw(currUser.getGroups());
		}
		
		// unlock
		mlock.unlock();
	}
	
	/**
	 * Add user in a group you specify 
	 * @param username
	 * @param groupname
	 * @param r
	 * @param g
	 * @param b
	 */
	public void addUserToGroup(String username, String groupname, int r, int g, int b) {
		// lock
		mlock.lock();
		
		User user = this.getUserByName(username);
		Group group = this.getGroupByName(groupname);
		Color color = new Color(r, g, b);
		
		group.addUser(user, color);
    	user.joinGroup(group);
    	
    	// workaround - to trigger the listener
		Group bullshit = new Group("o_O");
		groups.add(bullshit);
		groups.remove(bullshit);
		
		if (whiteboardController != null) {
			whiteboardController.redraw(currUser.getGroups());
		}
		
		// unlock
		mlock.unlock();
	}
	
	/**
	 * Add new shape to the canvas of the specified group(using username's color)
	 * @param username
	 * @param groupname
	 * @param shape
	 * @param x
	 * @param y
	 */
	public void drawShapeToGroup(String username, String groupname, String shape, int x, int y) {
		//lock
		mlock.lock();
		
		User user = this.getUserByName(username);
		Group group = this.getGroupByName(groupname);
		
		if (user != null && group != null) {
			Drawing drawingShape = null;
			Color color = group.getUserColor(user);
			
			if (shape.equals("Circle")) {
				drawingShape = new Circle(x, y, color);
			}
			if (shape.equals("Square")) {
				drawingShape = new Square(x, y, color);
			}
			if (shape.equals("Line")) {
				drawingShape = new Line(x, y, color);
			}
			if (shape.equals("Arrow")) {
				drawingShape = new Arrow(x, y, color);
			}
			group.logShape(drawingShape);
			
			if (whiteboardController != null) {
				whiteboardController.updateDrawings(group);
			}
		}
		
		//unlock
		mlock.unlock();
	}
	
	/**
	 * Add a new message to group
	 * @param groupname
	 * @param username
	 * @param message
	 */
	public void sendMessageToGroup(String username, String groupname, String message) {
		//lock
		mlock.lock();
		User user = this.getUserByName(username);
		Group group = this.getGroupByName(groupname);
		
		if (user != null && group != null) {
			Color color = group.getTextColor();
			String font = group.getFont();
			MessageInfo msgInfo = new MessageInfo(user, message, color, font);
			group.logMessage(msgInfo);
			
			if (whiteboardController != null) {
				whiteboardController.updateTextMessages(group);
			}
		}
		
		//unlock
		mlock.unlock();
	}
	
	/**
	 * Save messages from the current active groups
	 */
	public void saveGroupWork() {
		//lock
		mlock.lock();
		BufferedWriter output;
		String fileNamePath = this.getUserByName(username) + "_messages.txt";
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		
		try {
			output = new BufferedWriter(new FileWriter(fileNamePath, true));
			output.write("MESSAGES ON " + dateFormat.format(date) + "\n");
			
			for (Group group : this.getUserByName(username).getGroups()) {
				output.write("Within group <" + group.getName() + ">\n");
				for (MessageInfo msg : group.getLoggedMessages()) {
					output.write(msg.getUser() + ": " + msg.getMessage() + "\n");
				}
			}
			
			//docList.updateDocText("u1");
			
			output.close() ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//unlock
		mlock.unlock();
	}
	
	/**
	 * Update google doc file for the user username
	 * @param username
	 */
	public void updateXML(String username) {
		try {
			DocumentList docList = DocumentList.getInstance();
			
			for (Group group : mediator.getUserByName(username).getGroups()) {
				
				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
				Document doc = docBuilder.newDocument();
				createXMLDoc(doc, username, group);
				
				docList.updateDocText(group.getName());
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Generate local XML file to be uploaded to google doc
	 * @param doc
	 * @param username
	 * @param fileName
	 */
	private void createXMLDoc(Document doc, String username, Group group) {
		String fileName = group.getName() + "_messages.txt";
		Element root = doc.createElement("Whiteboard");
		doc.appendChild(root);

		Element groupChild = doc.createElement("Group_" + group.getName());
		
		Element messagesChild = doc.createElement("Messages");
		for (MessageInfo msg : group.getLoggedMessages()) {
			Element msgChild = doc.createElement("Message");
			Text msgContent = doc.createTextNode(msg.getUser() + ": " + msg.getMessage());
			msgChild.appendChild(msgContent);
			messagesChild.appendChild(msgChild);
		}
		groupChild.appendChild(messagesChild);
		
		Element drawingsChild = doc.createElement("Drawings");
		for (Drawing drw : group.getDrawings()) {
			Element drawingChild = doc.createElement("Shape");
			Text drawingProperties = doc.createTextNode(drw.toString());
			drawingChild.appendChild(drawingProperties);
			drawingsChild.appendChild(drawingChild);
		}
		
		groupChild.appendChild(drawingsChild);
		
		root.appendChild(groupChild);
		
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			 
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			  
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			
			transformer.transform(source, result);
			String xmlString = sw.toString();
			
			File file = new File(fileName);
			BufferedWriter bw = new BufferedWriter
			(new OutputStreamWriter(new FileOutputStream(file)));
			bw.write(xmlString);
			bw.flush();
			bw.close();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	/**
	 * Add controller for whiteboard
	 * @param whiteboardController
	 */
	public void addWhiteboardController(WhiteboardController whiteboardController) {
		this.whiteboardController = whiteboardController;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
	
}
